/**
 * 
 */
/**
 * @author TP1602008
 *
 */
package quickmobile_mobile;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.remote.MobilePlatform;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class test {

	AndroidDriver driver;
	Dimension size;

	@BeforeClass
	public void beforeClass() throws MalformedURLException {

		DesiredCapabilities caps = new DesiredCapabilities();
		caps.setCapability(MobileCapabilityType.PLATFORM_NAME, MobilePlatform.ANDROID);
		caps.setCapability(MobileCapabilityType.DEVICE_NAME, "Android device");
		caps.setCapability("appPackage", "com.quickmobile.app293");
		caps.setCapability("appActivity", "com.quickmobile.conference.ApplicationInitialLoadActivity");
		driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), caps);
		driver.manage().timeouts().implicitlyWait(1000, TimeUnit.SECONDS);

	}

	@AfterClass
	public void afterClass() {
	}
	
	@Test
	public void f() throws InterruptedException {

		WebDriverWait wait = new WebDriverWait(driver, 3);
		wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.ImageButton[@content-desc='OK']")));

		driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc='OK']")).click();

		size = driver.manage().window().getSize();
		int starty = (int) (size.height * 0.80);
		int endy = (int) (size.height * 0.20);
		int startx = size.width / 2;

		for (int second = 0; second <= 60; second++) {
			if (isElementPresent(By.xpath("//android.widget.TextView[@text='My Profile']")))
				break;
			else {
				driver.swipe(startx, starty, startx, endy, 3000);
				Thread.sleep(1000);
			}
		}
		driver.findElement(By.xpath("//android.widget.TextView[@text='My Profile']")).click();

	}

	public boolean isElementPresent(By by) {
		driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
		List<WebElement> list = driver.findElements(by);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		if (list.size() == 0) {
			return false;
		} else {
			// return list.get(0).isDisplayed();
			return true;
		}
	}

}
