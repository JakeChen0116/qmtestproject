package quickmobile_mobile;

import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.remote.MobilePlatform;


public class qm {
	
	public AndroidDriver<WebElement> driver;
	public WebElement element;
	public Dimension size;
	public boolean bool;
	public String[] array;
	public int starty = 0;
	public int endy = 0;
	public int startx = 0;
	public int second = 0;

	@Before
	public void setUp() throws Exception {
		DesiredCapabilities caps = new DesiredCapabilities();
		caps.setCapability(MobileCapabilityType.PLATFORM_NAME, MobilePlatform.ANDROID);
		caps.setCapability(MobileCapabilityType.DEVICE_NAME, "Nexus 5X");
		caps.setCapability("appPackage", "com.quickmobile.app293");
		caps.setCapability("appActivity", "com.quickmobile.conference.ApplicationInitialLoadActivity");
		driver = new AndroidDriver<WebElement>(new URL("http://127.0.0.1:4723/wd/hub"), caps);
		driver.manage().timeouts().implicitlyWait(1000, TimeUnit.SECONDS);
	}

	@After
	public void tearDown() throws Exception {
		driver.closeApp();
	}

	@Test
	public void feature1() {

	}
	
	@Test
	public void feature2() {

	}

	@Test
	public void feature3() {

	}
	
	public void back() {
		driver.navigate().back();
	}

	public boolean isElementPresent(By by) {
		driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
		List<WebElement> list = driver.findElements(by);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		if (list.size() == 0) {
			return false;
		} else {
			return true;
		}
	}

}
