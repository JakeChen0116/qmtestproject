package quickmobile_mobile;

import java.net.URL;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.remote.MobilePlatform;

public class quickmobile {

	public AndroidDriver<WebElement> driver;
	public WebElement element;
	public Dimension size;
	public boolean bool;

	@Before
	public void setUp() throws Exception {
		DesiredCapabilities capabilites = new DesiredCapabilities();
		capabilites.setCapability(MobileCapabilityType.PLATFORM_NAME, MobilePlatform.ANDROID);
		capabilites.setCapability(MobileCapabilityType.DEVICE_NAME, "Nexux 5X");
		capabilites.setCapability("appPackage", "com.quickmobile.app293");
		capabilites.setCapability("appActivity", "com.quickmobile.conference.ApplicationInitialLoadActivity");
		driver = new AndroidDriver<WebElement>(new URL("http://127.0.0.1:4723/wd/hub"), capabilites);
		driver.manage().timeouts().implicitlyWait(8000, TimeUnit.SECONDS);
	}

	@After
	public void tearDown() throws Exception {
		driver.closeApp();
	}

//	@Test
//	public void test() throws Exception {
//		WebElement threedotbutton = driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc='OK']"));
//
//		size = driver.manage().window().getSize();
//		int starty = (int) (size.height * 0.80);
//		int endy = (int) (size.height * 0.20);
//		int startx = size.width / 2;
//		int second = 0;
//
//		String[] array = new String[40];
//
//		array[0] = "Activity Feed";
//		array[1] = "Articles";
//		array[2] = "Check-In";
//		array[3] = "City Guide";
//		array[4] = "Community";
//		array[5] = "Contacts";
//		array[6] = "Documents";
//		array[7] = "Exhibitors";
//		array[8] = "Facebook";
//		array[9] = "Gallery";
//		array[10] = "Game Center";
//		array[11] = "HTML Page";
//		array[12] = "Info Booth";
//		array[13] = "Lead Generation";
//		array[14] = "Maps";
//		array[15] = "Messaging";
//		array[16] = "My Briefcase";
//		array[17] = "My Exhibitors";
//		array[18] = "My Notes";
//		array[19] = "My Profile";
//		array[20] = "My Schedule";
//		array[21] = "Quick Meetings";
//		array[22] = "Schedule";
//		array[23] = "Search";
//		array[24] = "Session Q&A";
//		array[25] = "Speakers";
//		array[26] = "SpeakOut";
//		array[27] = "Sponsors";
//		array[28] = "Surveys";
//		array[29] = "Tell a Friend";
//		array[30] = "Tracks";
//		array[31] = "Twitter";
//		array[32] = "Venues";
//		array[33] = "Video";
//		array[34] = "Web View";
//		array[35] = "Welcome Video";
//		array[36] = "What's On";
//		array[37] = "Attendees";
//
//		for (second = 0; second <= 60; second++) {
//			threedotbutton.click();
//
//			if (isElementPresent(By.xpath("//android.widget.TextView[@text='" + array[second] + "']"))) {
//				driver.findElement(By.xpath("//android.widget.TextView[@text='" + array[second] + "']")).click();
//				System.out.println(array[second] + " is Pass.");
//			}
//
//			else {
//				driver.swipe(startx, starty, startx, endy, 3000);
//				Thread.sleep(1000);
//				driver.findElement(By.xpath("//android.widget.TextView[@text='" + array[second] + "']")).click();
//			}
//
//			if (second == 13) {
//				driver.findElement(By.xpath("//android.widget.Button[@text='OK']")).click();
//				System.out.println(array[second] + " OK button is Pass.");
//				threedotbutton.click();
//			}
//
//			if (second == 29) {
//				back();
//			}
//			
//			if (second == 35) {
//				threedotbutton.click();
//				Thread.sleep(1000);
//				threedotbutton.click();
//			}
//		}
//	}

	@Test
	public void test2() throws Exception{
		Thread.sleep(2000);
		driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc='OK']")).click();
		String str = driver.findElement(By.xpath("//android.widget.TextView[@text='Articles']")).getText();
	}
	public void back() {
		driver.navigate().back();
	}

	public boolean isElementPresent(By by) {
		driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
		List<WebElement> list = driver.findElements(by);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		if (list.size() == 0) {
			return false;
		} else {
			return true;
		}
	}
}
