package training;

import static org.junit.Assert.*;

import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.remote.MobilePlatform;

public class calculator {

	public AndroidDriver<WebElement> driver;
	public int count = 0;

	@Before
	public void setUp() throws Exception {
		DesiredCapabilities capabilites = new DesiredCapabilities();
		capabilites.setCapability(MobileCapabilityType.PLATFORM_NAME, MobilePlatform.ANDROID);
		capabilites.setCapability(MobileCapabilityType.DEVICE_NAME, "Nexux 5X");
		capabilites.setCapability("appPackage", "com.google.android.calculator");
		capabilites.setCapability("appActivity", "com.android.calculator2.CalculatorGoogle");
		driver = new AndroidDriver<WebElement>(new URL("http://127.0.0.1:4723/wd/hub"), capabilites);
		driver.manage().timeouts().implicitlyWait(10L, TimeUnit.SECONDS);
	}

	@After
	public void tearDown() throws Exception {
		Thread.sleep(2000);
		System.out.println("Done.");
		driver.closeApp();
	}

	@Test
	public void cal() throws InterruptedException {
		WebElement digit_0 = driver.findElement(By.id("digit_0"));
		WebElement digit_1 = driver.findElement(By.id("digit_1"));
		WebElement digit_2 = driver.findElement(By.id("digit_2"));
		WebElement digit_3 = driver.findElement(By.id("digit_3"));
		WebElement digit_4 = driver.findElement(By.id("digit_4"));
		WebElement digit_5 = driver.findElement(By.id("digit_5"));
		WebElement digit_6 = driver.findElement(By.id("digit_6"));
		WebElement digit_7 = driver.findElement(By.id("digit_7"));
		WebElement digit_8 = driver.findElement(By.id("digit_8"));
		WebElement digit_9 = driver.findElement(By.id("digit_9"));
		WebElement op_add = driver.findElement(By.id("op_add"));
		WebElement eq = driver.findElement(By.id("eq"));

		while (true) {
			count++;
			if (count > 3) {
				break;
			}
			System.out.println("The " + count + " times");
			Thread.sleep(500);
			assertTrue(isElementClickable(By.id("digit_0")));
			digit_0.click();
			op_add.click();
			assertTrue(isElementClickable(By.id("digit_1")));
			digit_1.click();
			op_add.click();
			assertTrue(isElementClickable(By.id("digit_2")));
			digit_2.click();
			op_add.click();
			assertTrue(isElementClickable(By.id("digit_3")));
			digit_3.click();
			op_add.click();
			assertTrue(isElementClickable(By.id("digit_4")));
			digit_4.click();
			op_add.click();
			assertTrue(isElementClickable(By.id("digit_5")));
			digit_5.click();
			op_add.click();
			assertTrue(isElementClickable(By.id("digit_6")));
			digit_6.click();
			op_add.click();
			assertTrue(isElementClickable(By.id("digit_7")));
			digit_7.click();
			op_add.click();
			assertTrue(isElementClickable(By.id("digit_8")));
			digit_8.click();
			op_add.click();
			assertTrue(isElementClickable(By.id("digit_9")));
			digit_9.click();
			eq.click();
			assertTrue(isElementClickable(By.id("eq")));
			assertTrue(isElementPresent(By.id("result")));
		}
	}

	public boolean isElementPresent(By by) {
		try {
			System.out.println(driver.findElement(by).getText());
			System.out.println("Pass");
			return true;
		} catch (NoSuchElementException e) {
			System.out.println(e.getMessage());
			return false;
		}
	}

	public boolean isElementClickable(By by) {
		try {
			driver.findElement(by).isEnabled();
			System.out.println("button can clickable.");
			return true;
		} catch (NoSuchElementException e) {
			System.out.println(e.getMessage());
			return false;
		}
	}

}
