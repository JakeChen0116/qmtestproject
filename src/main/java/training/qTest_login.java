package training;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

public class qTest_login {

	FirefoxDriver driver = new FirefoxDriver();
	public int i = 0;
	public boolean bool;
	public String browser;
	public String qTest_url;
	public String myProfileLogout = ".//*[@id='avatarHeader']", myProfileLogout2 = ".//*[@id='log-out-link']";

	@Before
	public void setUp() throws Exception {
		qTest_url = "https://quickmobile.qtestnet.com";
	}

	@After
	public void tearDown() throws Exception {
		driver.close();
		driver.quit();
	}

	@Test
	public void test() throws Exception {
		driver.get(qTest_url);
		Login();
	}

	public void Login() throws Exception {
		driver.findElement(By.xpath("//*[@id='userName']")).sendKeys("Ting.Yen@quickmobile.com");
		driver.findElement(By.xpath("//*[@id='passwordPlaceholder']")).click();
		driver.findElement(By.xpath("//*[@id='password']")).sendKeys("WITS*8888");
		driver.findElement(By.xpath(".//*[@id='loginForm']/div/div[1]/a/img")).click();	
			
		for (i = 1; i < 4; i++) {
			WebElement element = driver
					.findElement(By.xpath("//*[@id='activeSessionTable']/tbody/tr[" + i + "]/td[3]"));			
			browser = element.getText();

			if (browser != "IE") {
				bool = true;
				break;
			}
		}

		if (bool == true) {
			Thread.sleep(1000);
			driver.findElement(By.xpath(".//*[@id='activeSessionTable']/tbody/tr[" + i + "]/td[8]/a/span")).click();
			driver.findElement(By.xpath(".//*[@id='reloginBtn']")).click();
			exit();
		} else {
			exit();
		}
	}

	public void exit() {
		driver.findElement(By.xpath(myProfileLogout)).click();
		driver.findElement(By.xpath(myProfileLogout2)).click();
	}

	public boolean isElementPresent(By by) {
		try {
			driver.findElement(by);
			return true;

		} catch (NoSuchElementException e) {
			return false;
		}
	}

	public boolean isElementNotPresent(By by) {
		try {
			driver.findElement(by);
			return false;

		} catch (NoSuchElementException e) {
			return true;
		}
	}

	public boolean isElementClickable(By by) {
		try {
			driver.findElement(by).isEnabled();
			return true;

		} catch (NoSuchElementException e) {
			return false;
		}
	}

	public boolean isElementNotClickable(By by) {
		try {
			driver.findElement(by).isEnabled();
			return false;

		} catch (NoSuchElementException e) {
			return true;
		}
	}
}
