package automation_mobile;

import java.net.URL;
import java.util.concurrent.TimeUnit;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.WebElement;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.remote.MobilePlatform;

public class calculator {

	AndroidDriver driver;
	int i = 0;

	@Before
	public void setUp() throws Exception {
		DesiredCapabilities capabilites = new DesiredCapabilities();
		capabilites.setCapability(MobileCapabilityType.PLATFORM_NAME, MobilePlatform.ANDROID);
		capabilites.setCapability(MobileCapabilityType.DEVICE_NAME, "Nexux 5X");
		capabilites.setCapability("appPackage", "com.quickmobile.app6171");
		capabilites.setCapability("appActivity", "com.quickmobile.conference.ApplicationInitialLoadActivity");
		driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilites);
		driver.manage().timeouts().implicitlyWait(20L, TimeUnit.SECONDS);
		
	}

	@Test
	public void test() throws InterruptedException {

		WebElement drawer = driver.findElement(By.className("android.widget.ImageButton"));
		Thread.sleep(1000);
		while (i <= 1000) {
			System.out.println("The" + i + "Number");
			i++;
			drawer.click();
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.closeApp();
	}

}
