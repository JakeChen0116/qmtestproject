package automation_mobile;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.remote.MobilePlatform;
import jxl.read.biff.BiffException;

public class Login {

	AndroidDriver<WebElement> driver;
	Dimension size;
	int counter = 0;

	@BeforeClass
	public void BeforeClass() throws MalformedURLException {
		DesiredCapabilities capabilites = new DesiredCapabilities();
		capabilites.setCapability(MobileCapabilityType.PLATFORM_NAME, MobilePlatform.ANDROID);
		capabilites.setCapability(MobileCapabilityType.DEVICE_NAME, "Nexux 5X");
		capabilites.setCapability("appPackage", "com.quickmobile.app6171");
		capabilites.setCapability("appActivity", "com.quickmobile.conference.ApplicationInitialLoadActivity");
		driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilites);
		driver.manage().timeouts().implicitlyWait(20L, TimeUnit.SECONDS);
	}

	@Test
	public void Test() throws InterruptedException, BiffException, IOException {
		ToggleMenu();
		// MyProfileLogin();
	}

	public void ToggleMenu() throws InterruptedException {
		Thread.sleep(2000);
		// Toggle Menu
		WebElement ImageButton = driver.findElementByXPath("//android.widget.ImageButton[@content-desc='OK']");
		ImageButton.click();
		// System.out.println("Counter = " + counter);
		while (true) {
			// Thread.sleep(3000);
			// ImageButton.click();
			Thread.sleep(2000);
			// back();
			// System.out.println("Counter = " + counter);
			if (counter == 0) {
				System.out.println("PASS");
				break;
			}
		}
		// ImageButton.click();
		size = driver.manage().window().getSize();
		int starty = (int) (size.height * 0.80);
		int endy = (int) (size.height * 0.20);
		int startx = size.width / 2;
		for (int second = 0; second <= 60; second++) {
			// List<WebElement> list =
			// driver.findElements(By.xpath("//android.widget.TextView[@text='My
			// Profile']"));
			// Boolean isPresent =
			// driver.findElements(By.xpath("//android.widget.TextView[@text='My
			// Profile']")).isEmpty();
			if (isElementPresent(By.xpath("//android.widget.TextView[@text='My Profile']")))
				break;
			else {
				driver.swipe(startx, starty, startx, endy, 3000);
				Thread.sleep(2000);
			}
		}

		driver.findElement(By.xpath("//android.widget.TextView[@text='My Profile']")).click();
	}

	// An implicit wait is to tell WebDriver to poll the DOM for a certain
	// amount of time when
	// trying to find an element or elements if they are not immediately
	// available. The default
	// setting is 0. Once set, the implicit wait is set for the life of the
	// WebDriver object instance.

	public boolean isElementPresent(By by) {
		driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
		List<WebElement> list = driver.findElements(by);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		if (list.size() == 0) {
			return false;
		} else {
			// return list.get(0).isDisplayed();
			return true;
		}
	}

	public void MyProfileLogin() throws InterruptedException, BiffException, IOException {

		WebElement threedot = driver.findElementByClassName("android.widget.ImageView");
		threedot.click();
		Thread.sleep(1000);
		WebElement Loginname = driver.findElementById("com.quickmobile.pushtesting:id/title");
		Loginname.click();
		Thread.sleep(1000);
		List<WebElement> classname = driver.findElementsByClassName("android.widget.EditText");
		WebElement LoginButton = driver
				.findElementByXPath("//android.widget.Button[@content-desc='loginSubmitButton']");
		// WebElement CancelButton = driver
		// .findElementByXPath("//android.widget.Button[@content-desc='cancelSubmitButton']");
		WebElement account = classname.get(0);
		WebElement password = classname.get(1);

		account.click();
		account.sendKeys("Jake");
		Thread.sleep(500);
		password.click();
		password.sendKeys("12");
		Thread.sleep(500);
		LoginButton.click();
		driver.findElementByXPath("//android.widget.Button[@text='OK']").click();
		back();
		back();

		WebElement threedot1 = driver.findElementByClassName("android.widget.ImageView");
		threedot1.click();
		Thread.sleep(1000);
		WebElement Loginname1 = driver.findElementById("com.quickmobile.pushtesting:id/title");
		Loginname1.click();
		Thread.sleep(1000);
		List<WebElement> classname1 = driver.findElementsByClassName("android.widget.EditText");
		WebElement LoginButton1 = driver
				.findElementByXPath("//android.widget.Button[@content-desc='loginSubmitButton']");
		// WebElement CancelButton = driver
		// .findElementByXPath("//android.widget.Button[@content-desc='cancelSubmitButton']");
		WebElement account1 = classname1.get(0);
		WebElement password1 = classname1.get(1);

		account1.click();
		account1.sendKeys("Jake");
		Thread.sleep(500);
		password1.click();
		password1.sendKeys("1234");
		Thread.sleep(500);
		LoginButton1.click();
		driver.findElementByXPath("//android.widget.Button[@text='OK']").click();

		// while (true) {
		// driver.findElement(By.xpath("//android.widget.EditText[@content-desc='loginUsernameTextBox']")).clear();
		// driver.findElement(By.xpath("//android.widget.EditText[@content-desc='loginUsernameTextBox']"))
		// .sendKeys("JaK");
		// driver.findElement(By.xpath("//android.widget.EditText[@content-desc='loginPasswordTextBox']")).clear();
		// driver.findElement(By.xpath("//android.widget.EditText[@content-desc='loginPasswordTextBox']"))
		// .sendKeys("123");
		// driver.findElement(By.xpath("//android.widget.Button[@content-desc='loginSubmitButton']")).click();
		// driver.findElementByXPath("//android.widget.Button[@text='OK']").click();
		// }

	}

	public void back() throws InterruptedException {
		Thread.sleep(1000);
		driver.navigate().back();
	}

	@AfterClass
	public void AfterClass() {
		driver.closeApp();
	}
}
