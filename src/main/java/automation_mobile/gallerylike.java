package automation_mobile;

import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.remote.MobilePlatform;

public class gallerylike {

	AndroidDriver<WebElement> driver;
	Dimension size;
	int counter = 0;
	int count = 1;

	@Before
	public void setUp() throws Exception {
		DesiredCapabilities capabilites = new DesiredCapabilities();
		capabilites.setCapability(MobileCapabilityType.PLATFORM_NAME, MobilePlatform.ANDROID);
		capabilites.setCapability(MobileCapabilityType.DEVICE_NAME, "Nexux 5X");
		capabilites.setCapability("appPackage", "com.quickmobile.pushtesting");
		capabilites.setCapability("appActivity", "com.quickmobile.conference.ApplicationInitialLoadActivity");
		driver = new AndroidDriver<WebElement>(new URL("http://127.0.0.1:4723/wd/hub"), capabilites);
		driver.manage().timeouts().implicitlyWait(20L, TimeUnit.SECONDS);
	}

	@After
	public void tearDown() throws Exception {
		driver.closeApp();
	}

	@Test
	public void test() throws InterruptedException {
		ToggleMenu();
		Gallery();
	}

	public void ToggleMenu() throws InterruptedException {
		Thread.sleep(2000);
		// Toggle Menu
		WebElement ImageButton = driver.findElementByXPath("//android.widget.ImageButton[@content-desc='OK']");
		ImageButton.click();

		while (true) {

			Thread.sleep(2000);

			if (counter == 0) {
				System.out.println("PASS");
				break;
			}
		}

		size = driver.manage().window().getSize();
		int starty = (int) (size.height * 0.80);
		int endy = (int) (size.height * 0.20);
		int startx = size.width / 2;
		for (int second = 0; second <= 60; second++) {

			if (isElementPresent(By.xpath("//android.widget.TextView[@text='Gallery']")))
				break;
			else {
				driver.swipe(startx, starty, startx, endy, 3000);
				Thread.sleep(2000);
			}
		}

		driver.findElement(By.xpath("//android.widget.TextView[@text='Gallery']")).click();
	}

	public void Gallery() throws InterruptedException {
		Thread.sleep(1000);
		driver.findElement(By.id("widgetBackground")).click();
		Thread.sleep(1000);
		driver.findElement(By.className("android.widget.RelativeLayout")).click();
		Thread.sleep(1000);
		for (int j = 1; j <= 2000; j++) {
			System.out.println("Count is : " + j);
			driver.findElement(By.id("photoLikeBtn")).click();
			Thread.sleep(1000);
		}
	}

	public boolean isElementPresent(By by) {
		driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
		List<WebElement> list = driver.findElements(by);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		if (list.size() == 0) {
			return false;
		} else {
			return true;
		}
	}
}
