package qmtestprojectNG;

//import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

public class Statistic_Download_qTest_JUnit {

	public ChromeDriver driver;
	public String qTest_url, qTest_iphonem1, qTest_iphonem2, qTest_ipadm1, qTest_ipadm2, qTest_androidm1,
			qTest_androidm2, qTest_webappm1, qTest_webappm2;

	@Before
	public void setUp() throws Exception {

		System.setProperty("webdriver.chrome.driver", "c:\\chromedriver.exe");
		driver = new ChromeDriver();
		// driver = new FirefoxDriver();
		qTest_url = "https://goo.gl/hlGnEX"; // qTest
		qTest_iphonem1 = "https://goo.gl/BF1YT8";
		qTest_iphonem2 = "https://goo.gl/l8Onr0";
		qTest_ipadm1 = "https://goo.gl/83cqLl";
		qTest_ipadm2 = "https://goo.gl/arrcVv";
		qTest_androidm1 = "https://goo.gl/Hq0XG1";
		qTest_androidm2 = "https://goo.gl/0LLGGp";
		qTest_webappm1 = "https://goo.gl/ALdb1F";
		qTest_webappm2 = "https://goo.gl/Lx4M80";
		driver.manage().timeouts().implicitlyWait(10000, TimeUnit.SECONDS);

	}

	@After
	public void tearDown() throws Exception {

		driver.quit();

	}

	@Test
	public void test() throws InterruptedException {

		driver.get(qTest_url);
		driver.findElement(By.cssSelector("#userName")).clear();
		driver.findElement(By.cssSelector("#userName")).click();
		driver.findElement(By.cssSelector("#userName")).sendKeys("Ting.Yen@quickmobile.com");
		driver.findElement(By.id("passwordPlaceholder")).click();
		driver.findElement(By.id("password")).click();
		driver.findElement(By.id("password")).clear();
		driver.findElement(By.id("password")).sendKeys("WITS*8888");
		driver.findElement(By.xpath(".//*[@id='loginForm']/div/div/div[1]/a/img")).click();

		Thread.sleep(2000);
		driver.get(qTest_iphonem1);
		driver.findElement(By.xpath(".//*[@id='btnExportTestCycle_label']")).click();

		Thread.sleep(2000);
		driver.get(qTest_iphonem2);
		driver.findElement(By.xpath(".//*[@id='btnExportTestCycle_label']")).click();

		Thread.sleep(2000);
		driver.get(qTest_ipadm1);
		driver.findElement(By.xpath(".//*[@id='btnExportTestCycle_label']")).click();

		Thread.sleep(2000);
		driver.get(qTest_ipadm2);
		driver.findElement(By.xpath(".//*[@id='btnExportTestCycle_label']")).click();

		Thread.sleep(2000);
		driver.get(qTest_androidm1);
		driver.findElement(By.xpath(".//*[@id='btnExportTestCycle_label']")).click();

		Thread.sleep(2000);
		driver.get(qTest_androidm2);
		driver.findElement(By.xpath(".//*[@id='btnExportTestCycle_label']")).click();

		Thread.sleep(2000);
		driver.get(qTest_webappm1);
		driver.findElement(By.xpath(".//*[@id='btnExportTestCycle_label']")).click();

		Thread.sleep(2000);
		driver.get(qTest_webappm2);
		driver.findElement(By.xpath(".//*[@id='btnExportTestCycle_label']")).click();

		Thread.sleep(2000);
		driver.findElement(By.cssSelector("#avatarHeader")).click();
		driver.findElement(By.cssSelector("#log-out-link")).click();
		Thread.sleep(1000);
		// fail("Not yet implemented");

	}

}
