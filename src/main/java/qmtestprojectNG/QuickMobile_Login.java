package qmtestprojectNG;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;

import java.io.File;
import java.util.Random;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.remote.MobilePlatform;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class QuickMobile_Login {

	AndroidDriver driver;

	@BeforeClass
	public void beforeClass() throws MalformedURLException {

		DesiredCapabilities caps = new DesiredCapabilities();
		caps.setCapability(MobileCapabilityType.PLATFORM_NAME, MobilePlatform.ANDROID);
		caps.setCapability(MobileCapabilityType.DEVICE_NAME, "Android device");
		caps.setCapability(MobileCapabilityType.APP_PACKAGE, "com.quickmobile.pushtesting");
		caps.setCapability(MobileCapabilityType.APP_ACTIVITY,
				"com.quickmobile.conference.ApplicationInitialLoadActivity");
		driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), caps);
		driver.manage().timeouts().implicitlyWait(10000, TimeUnit.SECONDS);

	}

	@Test
	public void Start() throws InterruptedException {

		 WebElement SpecialID =
		 driver.findElementById("com.quickmobile.pushtesting:id/snap_event_desc");
		 Thread.sleep(3000);
		 SpecialID.click();
		 Thread.sleep(1000);
		 	 
		 
		 Fail();
//		 Success();
		  
	}

	private void Fail() throws InterruptedException {
		// TODO Auto-generated method stub
		while (true) {
			WebElement Account = driver
					.findElementByXPath("//android.widget.EditText[@content-desc='loginUsernameTextBox']");
			Account.clear();
			Thread.sleep(1000);
			Account.sendKeys("J");
			// Thread.sleep(1000);
			WebElement Password = driver
					.findElementByXPath("//android.widget.EditText[@content-desc='loginPasswordTextBox']");
			Password.clear();
			Thread.sleep(1000);
			Password.sendKeys("1");
			// Thread.sleep(1000);
			WebElement Login = driver.findElementByXPath("//android.widget.Button[@content-desc='loginSubmitButton']");
			Login.click();
			// Thread.sleep(1000);
			WebElement ok = driver.findElement(By.id("android:id/button1"));
			ok.click();
		}
	}

	private void Success() throws InterruptedException {
		// TODO Auto-generated method stub
		 //Success login
		 WebElement Account = driver.findElementByXPath("//android.widget.EditText[@content-desc='loginUsernameTextBox']");
		 Account.sendKeys("Jake");
		 Thread.sleep(1000);
		 WebElement Password = driver.findElementByXPath("//android.widget.EditText[@content-desc='loginPasswordTextBox']");
		 Password.sendKeys("1234");
		 Thread.sleep(1000);
		 WebElement Login = driver.findElementByXPath("//android.widget.Button[@content-desc='loginSubmitButton']");
	     Login.click();
	     Thread.sleep(1000);
	}

	@AfterClass
	public void afterClass() {
//		driver.closeApp();
	}

}
