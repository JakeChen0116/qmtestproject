package qmtestprojectNG;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.remote.MobilePlatform;

public class NewTest 
{
	AndroidDriver driver;

	@BeforeClass
	public void setUp() throws MalformedURLException
	{
		DesiredCapabilities caps = new DesiredCapabilities();
		caps.setCapability(MobileCapabilityType.PLATFORM_NAME, MobilePlatform.ANDROID);
		caps.setCapability(MobileCapabilityType.DEVICE_NAME,"Android device");
		caps.setCapability(MobileCapabilityType.APP_PACKAGE,"com.quickmobile.pushtesting");
		caps.setCapability(MobileCapabilityType.APP_ACTIVITY,"com.quickmobile.conference.ApplicationInitialLoadActivity");
		driver = new AndroidDriver (new URL("http://127.0.0.1:4723/wd/hub"), caps);
		driver.manage().timeouts().implicitlyWait(10000, TimeUnit.SECONDS);
	}
	
	@Test
	public void testExample() throws InterruptedException
	{
		test1();
//		test2();
//		test3();
	}
			
	private void test1() throws InterruptedException {

		
		// TODO Auto-generated method stub
		// ComboBox 
				Thread.sleep(10000);			
				int counter = 0;
				System.out.println("Counter = " + counter);
				WebElement ComboBox = driver.findElement(By.id("android:id/text1"));	
				
				Thread.sleep(1000);			
				List<WebElement> List_LinearLayout = driver.findElementsByClassName("android.widget.LinearLayout");
				
				while (true) {
					
					ComboBox.click();
					Thread.sleep(1000);

					// Tap Announcement
					WebElement Announcement = List_LinearLayout.get(0);
					Announcement.click();
					Thread.sleep(1000);
					back();

					ComboBox.click();
					Thread.sleep(1000);

					// Tap Gallery
					WebElement Gallery = List_LinearLayout.get(1);
					Gallery.click();
					Thread.sleep(1000);
					back();

					ComboBox.click();
					Thread.sleep(1000);

					// Tap Schedule
					WebElement Schedule = List_LinearLayout.get(2);
					Schedule.click();
					Thread.sleep(1000);
					back();

					ComboBox.click();
					Thread.sleep(1000);

					// Tap Twitter
					WebElement Twitter = List_LinearLayout.get(3);
					Twitter.click();
					Thread.sleep(1000);
					back();
					
					counter++;
					System.out.println("Counter = " + counter);
					if (counter == 1) {
						System.out.println("PASS");
						break;
					}
				}
		
	}
	
	private void test2() throws InterruptedException {
		// TODO Auto-generated method stub
		// Toggle Menu
				List<WebElement> List_ToggleMenu = driver.findElementsByClassName("android.widget.ImageButton");
				
				WebElement ImageButton = List_ToggleMenu.get(0);
				int counter = 0;
				System.out.println("Counter = " + counter);
				while (true) {
					
					ImageButton.click();
					Thread.sleep(2000);
					back();
					counter++;
					System.out.println("Counter = " + counter);
					if (counter == 1) {
						System.out.println("PASS");
						break;
					}
				}
	}

	private void test3() throws InterruptedException {
		// TODO Auto-generated method stub
		// Context Menu
		Thread.sleep(2000);
		List<WebElement> List_ToggleMenu = driver.findElementsByClassName("android.widget.ImageButton");
		WebElement ImageButton = List_ToggleMenu.get(0);
		ImageButton.click();
		Thread.sleep(1000);
		List<WebElement> List_ContextMenu = driver.findElementsByClassName("android.widget.LinearLayout");
//		WebElement ActivityFeed = List_ContextMenu.get(0);
//		Thread.sleep(1000);
		WebElement Attendee = List_ContextMenu.get(0);
		Attendee.click();
		System.out.println("PASS");
		Thread.sleep(1000);
		
	}

	private void back() throws InterruptedException{
		// TODO Auto-generated method stub
		driver.navigate().back();
		Thread.sleep(2000);
	}

	@AfterClass
	public void tearDown()
	{
		driver.closeApp();
	}
}
