package qmtestprojectNG;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import org.openqa.selenium.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class SaveAutomation {

	public ChromeDriver driver;
	public int number = 0;

	@Before
	public void setUp() throws Exception {
		System.setProperty("webdriver.chrome.driver", "c:\\chromedriver.exe");
		driver = new ChromeDriver();
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

	@Test
	public void test() throws InterruptedException {
		for (number = 0; number < 5; number++) {
			driver.findElement(By.xpath("//*[@id=testRunStatus_selectNode]")).click();
			Thread.sleep(1000);
			driver.findElement(By.xpath("//*[@id=testRunStatus_selectNode]")).sendKeys("Passed");
			Thread.sleep(1000);
			driver.findElement(By.xpath("//*[@id=btnMarkAllTestStepStatus_label]")).click();
			Thread.sleep(1000);
			driver.findElement(By.xpath("//*[@id=testExecutionToolbarSave_label]")).click();
			Thread.sleep(1000);
		}
	}

}
