package qmtestprojectNG;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import java.util.concurrent.TimeUnit;

public class Statistic_Download_qTest_36Version {

	public String qTest_url, qTest_iphonem1, qTest_iphonem2, qTest_ipadm1, qTest_ipadm2, qTest_androidm1,
			qTest_androidm2, qTest_webappm1, qTest_webappm2;
	public String str = "//*[@id='activeSessionDialog' and contains (., 'Terminate Sessions')]/div/div/div[1]/h4";
	public WebDriver driver;

	@Before
	public void setUp() throws Exception {
		System.setProperty("webdriver.chrome.driver", "c:\\chromedriver.exe");
		driver = new ChromeDriver();
		qTest_url = "https://quickmobile.qtestnet.com/"; // qTest
		qTest_iphonem1 = "https://goo.gl/0Gl0zY";
		qTest_iphonem2 = "https://goo.gl/OkVOYV";
		qTest_ipadm1 = "https://goo.gl/Scfdcl";
		qTest_ipadm2 = "https://goo.gl/H4NA9S";
		qTest_androidm1 = "https://goo.gl/sHnPpk";
		qTest_androidm2 = "https://goo.gl/eYXeJa";
		qTest_webappm1 = "https://goo.gl/0sZPKJ";
		qTest_webappm2 = "https://goo.gl/3CJmLd";
		driver.manage().timeouts().implicitlyWait(1000, TimeUnit.SECONDS);
	}

	@Test
	public void test() throws InterruptedException {
		driver.get(qTest_url);
		login();
		// Thread.sleep(1500);
		// if (driver.findElement(By.xpath(str)).isSelected()) {
		// driver.findElement(By.xpath("//*[@id='activeSessionTable']/tbody/tr[1]/td[8]/a/span")).click();
		// driver.findElement(By.xpath("//*[@id='reloginBtn']")).click();
		// download_url();
		// } else {
		// download_url();
		// }
		download_url();
	}

	public void login() throws InterruptedException {
		driver.findElement(By.cssSelector("#userName")).clear();
		driver.findElement(By.cssSelector("#userName")).click();
		driver.findElement(By.cssSelector("#userName")).sendKeys("Ting.Yen@quickmobile.com");
		driver.findElement(By.id("passwordPlaceholder")).click();
		driver.findElement(By.id("password")).click();
		driver.findElement(By.id("password")).clear();
		driver.findElement(By.id("password")).sendKeys("WITS*8888");
		driver.findElement(By.xpath("//*[@id='loginForm']/div/div[1]/a/img")).click();
	}

	public void download_url() throws InterruptedException {
		String str_DownloadButton = "//*[@id='btnExportTestCycle_label']";
		driver.get(qTest_iphonem1);
		Thread.sleep(3000);
		driver.findElement(By.xpath(str_DownloadButton)).click();
		Thread.sleep(3000);
		driver.get(qTest_iphonem2);
		Thread.sleep(3000);
		driver.findElement(By.xpath(str_DownloadButton)).click();
		Thread.sleep(3000);
		driver.get(qTest_ipadm1);
		Thread.sleep(3000);
		driver.findElement(By.xpath(str_DownloadButton)).click();
		Thread.sleep(3000);
		driver.get(qTest_ipadm2);
		Thread.sleep(3000);
		driver.findElement(By.xpath(str_DownloadButton)).click();
		Thread.sleep(3000);
		driver.get(qTest_androidm1);
		Thread.sleep(3000);
		driver.findElement(By.xpath(str_DownloadButton)).click();
		Thread.sleep(3000);
		driver.get(qTest_androidm2);
		Thread.sleep(3000);
		driver.findElement(By.xpath(str_DownloadButton)).click();
		Thread.sleep(3000);
		driver.get(qTest_webappm1);
		Thread.sleep(3000);
		driver.findElement(By.xpath(str_DownloadButton)).click();
		Thread.sleep(3000);
		driver.get(qTest_webappm2);
		Thread.sleep(3000);
		driver.findElement(By.xpath(str_DownloadButton)).click();
		Thread.sleep(3000);
		driver.findElement(By.cssSelector("#avatarHeader")).click();
		driver.findElement(By.cssSelector("#log-out-link")).click();
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

}
