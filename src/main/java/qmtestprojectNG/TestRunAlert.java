package qmtestprojectNG;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import javax.mail.*;
import javax.mail.internet.*;

public class TestRunAlert {

	public String qTest_url, WITS2ndPass_url, iphone_url, ipad_url, android_url, webapp_url;
	public String iphoneM1_url, ipadM1_url, androidM1_url, webappM1_url;
	public String iphoneM2_url, ipadM2_url, androidM2_url, webappM2_url;
	
	public String totalTestRun, totalTestUnexecuted, totalTestExecuted, totalTestPassed, totalTestFailed, totalTestIncomplete;
	public String iphoneTestRun, iphoneTestUnexecuted, iphoneTestExecuted, iphoneTestPassed, iphoneTestFailed, iphoneTestIncomplete;
	public String ipadTestRun, ipadTestUnexecuted, ipadTestExecuted, ipadTestPassed, ipadTestFailed, ipadTestIncomplete;
	public String androidTestRun, androidTestUnexecuted, androidTestExecuted,androidTestPassed, androidTestFailed, androidTestIncomplete;
	public String webappTestRun, webappTestUnexecuted, webappTestExecuted, webappTestPassed, webappTestFailed, webappTestIncomplete;
	
	public int totalTestRunM1, totalTestUnexecutedM1, totalTestExecutedM1, totalTestPassedM1, totalTestFailedM1, totalTestIncompleteM1;
	public String iphoneTestRunM1, iphoneTestUnexecutedM1, iphoneTestExecutedM1, iphoneTestPassedM1, iphoneTestFailedM1, iphoneTestIncompleteM1;
	public String ipadTestRunM1, ipadTestUnexecutedM1, ipadTestExecutedM1, ipadTestPassedM1, ipadTestFailedM1, ipadTestIncompleteM1;
	public String androidTestRunM1, androidTestUnexecutedM1, androidTestExecutedM1,androidTestPassedM1, androidTestFailedM1, androidTestIncompleteM1;
	public String webappTestRunM1, webappTestUnexecutedM1, webappTestExecutedM1, webappTestPassedM1, webappTestFailedM1, webappTestIncompleteM1;
	
	public int totalTestRunM2, totalTestUnexecutedM2, totalTestExecutedM2, totalTestPassedM2, totalTestFailedM2, totalTestIncompleteM2;
	public String iphoneTestRunM2, iphoneTestUnexecutedM2, iphoneTestExecutedM2, iphoneTestPassedM2, iphoneTestFailedM2, iphoneTestIncompleteM2;
	public String ipadTestRunM2, ipadTestUnexecutedM2, ipadTestExecutedM2, ipadTestPassedM2, ipadTestFailedM2, ipadTestIncompleteM2;
	public String androidTestRunM2, androidTestUnexecutedM2, androidTestExecutedM2,androidTestPassedM2, androidTestFailedM2, androidTestIncompleteM2;
	public String webappTestRunM2, webappTestUnexecutedM2, webappTestExecutedM2, webappTestPassedM2, webappTestFailedM2, webappTestIncompleteM2;
	
	public String myProfileLogout = ".//*[@id='avatarHeader']", myProfileLogout2 = ".//*[@id='log-out-link']";
	public WebDriver driver = new FirefoxDriver();
	public String zero = "0";
	
	@Before
	public void setUp() throws Exception {
		qTest_url = "https://quickmobile.qtestnet.com"; // qTest
		WITS2ndPass_url = "https://goo.gl/ML9N1Y"; // WITS-2nd Pass URL
		
		iphone_url = "https://goo.gl/N5cQQQ"; // iPhone URL
		ipad_url = "https://goo.gl/14rPBC"; // iPad URL
		android_url = "https://goo.gl/u1NUUN"; // Android URL
		webapp_url = "https://goo.gl/neHRJW"; // WebApp URL
		
		iphoneM1_url = "https://goo.gl/r0doWW"; // iPhonem1 URL
		ipadM1_url = "https://goo.gl/F43Y2H"; // iPadm1 URL
		androidM1_url = "https://goo.gl/mRT0kg"; // Androidm1 URL
		webappM1_url = "https://goo.gl/oe1HyY"; // WebAppm1 URL
		
		iphoneM2_url = "https://goo.gl/NEJ8pX"; // iPhonem2 URL
		ipadM2_url = "https://goo.gl/IA1G6M"; // iPadm2 URL
		androidM2_url = "https://goo.gl/QbLq4E"; // Androidm2 URL
		webappM2_url = "https://goo.gl/W9XEJZ"; // WebAppm2 URL
		
		driver.manage().timeouts().implicitlyWait(1000, TimeUnit.SECONDS);
	}

	@Test
	public void test() throws InterruptedException, AddressException, MessagingException {
		driver.get(qTest_url);
		Login();
		Calculator(); 
		CalculatorM1();
		CalculatorM2();
		SendMail();
	}

	public void Login() throws InterruptedException {
		driver.findElement(By.xpath("//*[@id='userName']")).clear();
		driver.findElement(By.xpath("//*[@id='userName']")).click();
		driver.findElement(By.xpath("//*[@id='userName']")).sendKeys("Ting.Yen@quickmobile.com");
		driver.findElement(By.xpath("//*[@id='passwordPlaceholder']")).click();
		driver.findElement(By.xpath("//*[@id='password']")).clear();
		driver.findElement(By.xpath("//*[@id='password']")).sendKeys("WITS*8888");
		Thread.sleep(1000);
		driver.findElement(By.xpath(".//*[@id='loginForm']/div/div[1]/a/img")).click();
	}
	
	public void Calculator() throws AddressException, MessagingException, InterruptedException {
		driver.get(WITS2ndPass_url);
		totalTestRun = driver.findElement(By.xpath(".//*[@id='sumTestRunTotal']")).getText();
		Thread.sleep(2000);
//		totalTestUnexecuted = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[5]/div/div[2]/span")).getText();
		totalTestExecuted = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[1]/div/div[2]/span")).getText();
		totalTestPassed = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[2]/div/div[2]/span")).getText();
		totalTestFailed = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[3]/div/div[2]/span")).getText();
		totalTestIncomplete = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[4]/div/div[2]/span")).getText();
		
		driver.get(iphone_url);
		Thread.sleep(8000);
		iphoneTestRun = driver.findElement(By.xpath(".//*[@id='sumTestRunTotal']")).getText();
//		iphoneTestUnexecuted = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[5]/div/div[2]/span")).getText();
		iphoneTestExecuted = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[1]/div/div[2]/span")).getText();
		iphoneTestPassed = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[2]/div/div[2]/span")).getText();
		iphoneTestFailed = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[3]/div/div[2]/span")).getText();
		iphoneTestIncomplete = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[4]/div/div[2]/span")).getText();
		
		driver.get(ipad_url);
		Thread.sleep(8000);
		ipadTestRun = driver.findElement(By.xpath(".//*[@id='sumTestRunTotal']")).getText();
//		ipadTestUnexecuted = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[5]/div/div[2]/span")).getText();
		ipadTestExecuted = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[1]/div/div[2]/span")).getText();
		ipadTestPassed = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[2]/div/div[2]/span")).getText();
		ipadTestFailed = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[3]/div/div[2]/span")).getText();
		ipadTestIncomplete = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[4]/div/div[2]/span")).getText();
		
		driver.get(android_url);
		Thread.sleep(8000);
		androidTestRun = driver.findElement(By.xpath(".//*[@id='sumTestRunTotal']")).getText();
//		androidTestUnexecuted = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[5]/div/div[2]/span")).getText();
		androidTestExecuted = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[1]/div/div[2]/span")).getText();
		androidTestPassed = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[2]/div/div[2]/span")).getText();
		androidTestFailed = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[3]/div/div[2]/span")).getText();
		androidTestIncomplete = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[4]/div/div[2]/span")).getText();
		
		driver.get(webapp_url);
		Thread.sleep(8000);
		webappTestRun = driver.findElement(By.xpath(".//*[@id='sumTestRunTotal']")).getText();
//		webappTestUnexecuted = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[5]/div/div[2]/span")).getText();
		webappTestExecuted = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[1]/div/div[2]/span")).getText();
		webappTestPassed = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[2]/div/div[2]/span")).getText();
		webappTestFailed = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[3]/div/div[2]/span")).getText();
		webappTestIncomplete = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[4]/div/div[2]/span")).getText();
		
	}

	public void CalculatorM1() throws AddressException, MessagingException, InterruptedException{
		driver.get(iphoneM1_url);
		Thread.sleep(8000);
		iphoneTestRunM1 = driver.findElement(By.xpath(".//*[@id='sumTestRunTotal']")).getText();
//		iphoneTestUnexecutedM1 = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[5]/div/div[2]/span")).getText();
		iphoneTestExecutedM1 = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[1]/div/div[2]/span")).getText();
		iphoneTestPassedM1 = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[2]/div/div[2]/span")).getText();
		iphoneTestFailedM1 = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[3]/div/div[2]/span")).getText();
		iphoneTestIncompleteM1 = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[4]/div/div[2]/span")).getText();
		
		driver.get(ipadM1_url);
		Thread.sleep(8000);
		ipadTestRunM1 = driver.findElement(By.xpath(".//*[@id='sumTestRunTotal']")).getText();
//		ipadTestUnexecutedM1 = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[5]/div/div[2]/span")).getText();
		ipadTestExecutedM1 = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[1]/div/div[2]/span")).getText();
		ipadTestPassedM1 = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[2]/div/div[2]/span")).getText();
		ipadTestFailedM1 = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[3]/div/div[2]/span")).getText();
		ipadTestIncompleteM1 = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[4]/div/div[2]/span")).getText();
		
		driver.get(androidM1_url);
		Thread.sleep(8000);
		androidTestRunM1 = driver.findElement(By.xpath(".//*[@id='sumTestRunTotal']")).getText();
//		androidTestUnexecutedM1 = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[5]/div/div[2]/span")).getText();
		androidTestExecutedM1 = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[1]/div/div[2]/span")).getText();
		androidTestPassedM1 = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[2]/div/div[2]/span")).getText();
		androidTestFailedM1 = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[3]/div/div[2]/span")).getText();
		androidTestIncompleteM1 = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[4]/div/div[2]/span")).getText();
		
		driver.get(webappM1_url);
		Thread.sleep(8000);
		webappTestRunM1 = driver.findElement(By.xpath(".//*[@id='sumTestRunTotal']")).getText();
//		webappTestUnexecutedM1 = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[5]/div/div[2]/span")).getText();
		webappTestExecutedM1 = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[1]/div/div[2]/span")).getText();
		webappTestPassedM1 = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[2]/div/div[2]/span")).getText();
		webappTestFailedM1 = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[3]/div/div[2]/span")).getText();
		webappTestIncompleteM1 = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[4]/div/div[2]/span")).getText();
		
		totalTestRunM1 = Integer.parseInt(iphoneTestRunM1) + Integer.parseInt(ipadTestRunM1) + Integer.parseInt(androidTestRunM1) + Integer.parseInt(webappTestRunM1);
//		totalTestUnexecutedM1 = Integer.parseInt(iphoneTestUnExecutedM1) + Integer.parseInt(ipadTestUnExecutedM1) + Integer.parseInt(androidTestUnExecutedM1) + Integer.parseInt(webappTestUnexecutedM1);
		totalTestUnexecutedM1 = 0;
		totalTestExecutedM1 = Integer.parseInt(iphoneTestExecutedM1) + Integer.parseInt(ipadTestExecutedM1) + Integer.parseInt(androidTestExecutedM1) + Integer.parseInt(webappTestExecutedM1); 
		totalTestPassedM1 = Integer.parseInt(iphoneTestPassedM1) + Integer.parseInt(ipadTestPassedM1) + Integer.parseInt(androidTestPassedM1) + Integer.parseInt(webappTestPassedM1);
		totalTestFailedM1 = Integer.parseInt(iphoneTestFailedM1) + Integer.parseInt(ipadTestFailedM1) + Integer.parseInt(androidTestFailedM1) + Integer.parseInt(webappTestFailedM1);
		totalTestIncompleteM1 = Integer.parseInt(iphoneTestIncompleteM1) + Integer.parseInt(ipadTestIncompleteM1) + Integer.parseInt(androidTestIncompleteM1) + Integer.parseInt(webappTestIncompleteM1);
	}
	
	public void CalculatorM2() throws AddressException, MessagingException, InterruptedException{
		driver.get(iphoneM2_url);
		Thread.sleep(8000);
		iphoneTestRunM2 = driver.findElement(By.xpath(".//*[@id='sumTestRunTotal']")).getText();
//		iphoneTestUnexecutedM2 = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[5]/div/div[2]/span")).getText();
		iphoneTestExecutedM2 = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[1]/div/div[2]/span")).getText();
		iphoneTestPassedM2 = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[2]/div/div[2]/span")).getText();
		iphoneTestFailedM2 = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[3]/div/div[2]/span")).getText();
		iphoneTestIncompleteM2 = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[4]/div/div[2]/span")).getText();
		
		driver.get(ipadM2_url);
		Thread.sleep(8000);
		ipadTestRunM2 = driver.findElement(By.xpath(".//*[@id='sumTestRunTotal']")).getText();
//		ipadTestUnexecutedM2 = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[5]/div/div[2]/span")).getText();
		ipadTestExecutedM2 = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[1]/div/div[2]/span")).getText();
		ipadTestPassedM2 = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[2]/div/div[2]/span")).getText();
		ipadTestFailedM2 = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[3]/div/div[2]/span")).getText();
		ipadTestIncompleteM2 = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[4]/div/div[2]/span")).getText();
		
		driver.get(androidM2_url);
		Thread.sleep(8000);
		androidTestRunM2 = driver.findElement(By.xpath(".//*[@id='sumTestRunTotal']")).getText();
//		androidTestUnexecutedM2 = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[5]/div/div[2]/span")).getText();
		androidTestExecutedM2 = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[1]/div/div[2]/span")).getText();
		androidTestPassedM2 = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[2]/div/div[2]/span")).getText();
		androidTestFailedM2 = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[3]/div/div[2]/span")).getText();
		androidTestIncompleteM2 = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[4]/div/div[2]/span")).getText();
		
		driver.get(webappM2_url);
		Thread.sleep(8000);
		webappTestRunM2 = driver.findElement(By.xpath(".//*[@id='sumTestRunTotal']")).getText();
//		webappTestUnexecutedM2 = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[5]/div/div[2]/span")).getText();
		webappTestExecutedM2 = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[1]/div/div[2]/span")).getText();
		webappTestPassedM2 = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[2]/div/div[2]/span")).getText();
		webappTestFailedM2 = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[3]/div/div[2]/span")).getText();
		webappTestIncompleteM2 = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[4]/div/div[2]/span")).getText();	
		
		totalTestRunM2 = Integer.parseInt(iphoneTestRunM2) + Integer.parseInt(ipadTestRunM2) + Integer.parseInt(androidTestRunM2) + Integer.parseInt(webappTestRunM2);
//		totalTestUnexecutedM2 = Integer.parseInt(iphoneTestUnExecutedM2) + Integer.parseInt(ipadTestUnExecutedM2) + Integer.parseInt(androidTestUnExecutedM2) + Integer.parseInt(webappTestUnexecutedM2);
		totalTestUnexecutedM2 = 0;
		totalTestExecutedM2 = Integer.parseInt(iphoneTestExecutedM2) + Integer.parseInt(ipadTestExecutedM2) + Integer.parseInt(androidTestExecutedM2) + Integer.parseInt(webappTestExecutedM2); 
		totalTestPassedM2 = Integer.parseInt(iphoneTestPassedM2) + Integer.parseInt(ipadTestPassedM2) + Integer.parseInt(androidTestPassedM2) + Integer.parseInt(webappTestPassedM2);
		totalTestFailedM2 = Integer.parseInt(iphoneTestFailedM2) + Integer.parseInt(ipadTestFailedM2) + Integer.parseInt(androidTestFailedM2) + Integer.parseInt(webappTestFailedM2);
		totalTestIncompleteM2 = Integer.parseInt(iphoneTestIncompleteM2) + Integer.parseInt(ipadTestIncompleteM2) + Integer.parseInt(androidTestIncompleteM2) + Integer.parseInt(webappTestIncompleteM2);
	}
	
	public void SendMail() throws AddressException, MessagingException {
		try{
		String host = "tpma2001.wistronits.com";
		String from = "JakeChen@wistronits.com";
		String to = "JakeChen@wistronits.com";
		long time = System.currentTimeMillis();
		Date date = new Date(time);
		Properties props = System.getProperties();
		props.put("mail.smtp.host", host);
		Session mailSession = Session.getInstance(props, null);
		Message mailMessage = new MimeMessage(mailSession);
		mailMessage.setFrom(new InternetAddress(from));
		mailMessage.setRecipient(Message.RecipientType.TO, new InternetAddress(to));
		mailMessage.setSentDate(date);
		mailMessage.setSubject("QuickMobile->3.5->Drive to zero");
//		mailMessage.setText("Hi all,\n\nThis is a test mail, please ignore this mail.\n\nJake.");
		StringBuffer sb = new StringBuffer();
		sb.append("<h1>Drive to zero:</h1>\n");
//		sb.append("<p>Please refer to below table for the statistics of the testing items, thanks.<br><br></p>");
		sb.append("<p>Hi LingFen, thanks for your suggestion~<br>I have just finished modifing it. Please take a look, thanks.<br></p>");
		sb.append("<!DOCTYPE html>");
		sb.append("<html>");
		sb.append("<head>");
		sb.append("<style>");
		sb.append("table{border-collapse: collapse;width: 70%;}");
		sb.append("th{text-align: right;padding: 12px;background-color: #74e8e9;color: black;}");
		sb.append("td{text-align: right;padding: 9px;color: black;}");
		sb.append(".totaltestrun{background-color:#e0e0e0;}");
		sb.append("</style>");
		sb.append("</head>");
		sb.append("<body>");
		/*Total*/
		sb.append("<table border=\"1\">");
		sb.append("<tr><th><span style=color:red;>ALL</span></th><th>Test Runs</th><th>Test Executed</th><th>Unexecuted</th><th>Passed</th><th>Failed</th><th>Incomplete</th></tr>");
		sb.append("<tr><td>iPhone</td><td>"+iphoneTestRun+"</td><td>"+iphoneTestExecuted+"</td><td><span style=color:red;>"+zero+"</span></td><td>"+iphoneTestPassed+"</td><td>"+iphoneTestFailed+"</td><td><span style=color:#ff8000;>"+iphoneTestIncomplete+"</span></td></tr>");
		sb.append("<tr><td>iPad</td><td>"+ipadTestRun+"</td><td>"+ipadTestExecuted+"</td><td><span style=color:red;>"+zero+"</span></td><td>"+ipadTestPassed+"</td><td>"+ipadTestFailed+"</td><td><span style=color:#ff8000;>"+ipadTestIncomplete+"</span></td></tr>");
		sb.append("<tr><td>Android</td><td>"+androidTestRun+"</td><td>"+androidTestExecuted+"</td><td><span style=color:red;>"+zero+"</span></td><td>"+androidTestPassed+"</td><td>"+androidTestFailed+"</td><td><span style=color:#ff8000;>"+androidTestIncomplete+"</span></td></tr>");
		sb.append("<tr><td>WebApp</td><td>"+webappTestRun+"</td><td>"+webappTestExecuted+"</td><td><span style=color:red;>"+zero+"</span></td><td>"+webappTestPassed+"</td><td>"+webappTestFailed+"</td><td><span style=color:#ff8000;>"+webappTestIncomplete+"</span></td></tr>");		
		sb.append("<tr class=\"totaltestrun\"><td>Total</td><td>"+totalTestRun+"</td><td>"+totalTestExecuted+"</td><td>"+zero+"</td><td>"+totalTestPassed+"</td><td>"+totalTestFailed+"</td><td>"+totalTestIncomplete+"</td></tr>");
		sb.append("</table>");
		sb.append("<br>");
		/*M1*/
		sb.append("<table border=\"1\">");
		sb.append("<tr><th><span style=color:blue;>M1</span></th><th>Test Runs</th><th>Test Executed</th><th>Unexecuted</th><th>Passed</th><th>Failed</th><th>Incomplete</th></tr>");
		sb.append("<tr><td>iPhone</td><td>"+iphoneTestRunM1+"</td><td>"+iphoneTestExecutedM1+"</td><td><span style=color:red;>"+zero+"</span></td><td>"+iphoneTestPassedM1+"</td><td>"+iphoneTestFailedM1+"</td><td><span style=color:#ff8000;>"+iphoneTestIncompleteM1+"</span></td></tr>");
		sb.append("<tr><td>iPad</td><td>"+ipadTestRunM1+"</td><td>"+ipadTestExecutedM1+"</td><td><span style=color:red;>"+zero+"</span></td><td>"+ipadTestPassedM1+"</td><td>"+ipadTestFailedM1+"</td><td><span style=color:#ff8000;>"+ipadTestIncompleteM1+"</span></td></tr>");
		sb.append("<tr><td>Android</td><td>"+androidTestRunM1+"</td><td>"+androidTestExecutedM1+"</td><td><span style=color:red;>"+zero+"</span></td><td>"+androidTestPassedM1+"</td><td>"+androidTestFailedM1+"</td><td><span style=color:#ff8000;>"+androidTestIncompleteM1+"</span></td></tr>");
		sb.append("<tr><td>Webapp</td><td>"+webappTestRunM1+"</td><td>"+webappTestExecutedM1+"</td><td><span style=color:red;>"+zero+"</span></td><td>"+webappTestPassedM1+"</td><td>"+webappTestFailedM1+"</td><td><span style=color:#ff8000;>"+webappTestIncompleteM1+"</span></td></tr>");	
		sb.append("<tr class=\"totaltestrun\"><td>Sub-Total</td><td>"+Integer.toString(totalTestRunM1)+"</td><td>"+Integer.toString(totalTestExecutedM1)+"</td><td>"+Integer.toString(totalTestUnexecutedM1)+"</td><td>"+Integer.toString(totalTestPassedM1)+"</td><td>"+Integer.toString(totalTestFailedM1)+"</td><td>"+Integer.toString(totalTestIncompleteM1)+"</td></tr>");	
		sb.append("</table>");
		sb.append("<br>");
		/*M2*/
		sb.append("<table border=\"1\">");
		sb.append("<tr><th><span style=color:blue;>M2</span></th><th>Test Runs</th><th>Test Executed</th><th>Unexecuted</th><th>Passed</th><th>Failed</th><th>Incomplete</th></tr>");
		sb.append("<tr><td>iPhone</td><td>"+iphoneTestRunM2+"</td><td>"+iphoneTestExecutedM2+"</td><td><span style=color:red;>"+zero+"</span></td><td>"+iphoneTestPassedM2+"</td><td>"+iphoneTestFailedM2+"</td><td><span style=color:#ff8000;>"+iphoneTestIncompleteM2+"</span></td></tr>");
		sb.append("<tr><td>iPad</td><td>"+ipadTestRunM2+"</td><td>"+ipadTestExecutedM2+"</td><td><span style=color:red;>"+zero+"</span></td><td>"+ipadTestPassedM2+"</td><td>"+ipadTestFailedM2+"</td><td><span style=color:#ff8000;>"+ipadTestIncompleteM2+"</span></td></tr>");
		sb.append("<tr><td>Android</td><td>"+androidTestRunM2+"</td><td>"+androidTestExecutedM2+"</td><td><span style=color:red;>"+zero+"</span></td><td>"+androidTestPassedM2+"</td><td>"+androidTestFailedM2+"</td><td><span style=color:#ff8000;>"+androidTestIncompleteM2+"</span></td></tr>");
		sb.append("<tr><td>Webapp</td><td>"+webappTestRunM2+"</td><td>"+webappTestExecutedM2+"</td><td><span style=color:red;>"+zero+"</span></td><td>"+webappTestPassedM2+"</td><td>"+webappTestFailedM2+"</td><td><span style=color:#ff8000;>"+webappTestIncompleteM2+"</span></td></tr>");
		sb.append("<tr class=\"totaltestrun\"><td>Sub-Total</td><td>"+Integer.toString(totalTestRunM2)+"</td><td>"+Integer.toString(totalTestExecutedM2)+"</td><td>"+Integer.toString(totalTestUnexecutedM2)+"</td><td>"+Integer.toString(totalTestPassedM2)+"</td><td>"+Integer.toString(totalTestFailedM2)+"</td><td>"+Integer.toString(totalTestIncompleteM2)+"</td></tr>");	
		sb.append("</table>");
		sb.append("<p>Data Source: qTest.</p></body></html>");
		mailMessage.setContent(sb.toString(), "text/html");
		Transport.send(mailMessage);
		System.out.println("\n Mail was sent successfully.");
		}
		catch (Exception e){
			System.out.println(e.getMessage());
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.findElement(By.xpath(myProfileLogout)).click();
		driver.findElement(By.xpath(myProfileLogout2)).click();
		driver.quit();
	}

}
