package qmtestprojectNG;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import java.util.concurrent.TimeUnit;

public class JIRA_Export_Excel {

	public String JIRA;
	public String Issue = "https://quickmobile.atlassian.net/issues/";
	public String Export_xpath = "//*[@id='content' and contains (., 'Export')]/div[1]/div[3]/div/header/div[1]/ul/li[2]/a/span[2]";
	public String Excel_export = "//*[@id='allExcelFields']";
	public WebDriver driver;

	@Before
	public void setUp() throws Exception {
		System.setProperty("webdriver.chrome.driver", "c:\\chromedriver.exe");
		driver = new ChromeDriver();
		JIRA = "https://quickmobile.atlassian.net/"; // JIRA
		driver.manage().timeouts().implicitlyWait(1000, TimeUnit.SECONDS);
	}

	@Test
	public void test() throws InterruptedException {
		driver.get(JIRA);
		driver.findElement(By.xpath("//*[@id='username']")).clear();
		driver.findElement(By.xpath("//*[@id='username']")).click();
		driver.findElement(By.xpath("//*[@id='username']")).sendKeys("Jake.Chen");
		driver.findElement(By.xpath("//*[@id='password']")).clear();
		driver.findElement(By.xpath("//*[@id='password']")).click();
		driver.findElement(By.xpath("//*[@id='password']")).sendKeys("0939925215");
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='login']")).isDisplayed();
		driver.findElement(By.xpath("//*[@id='login']")).click();
		driver.get(Issue);
		Thread.sleep(1000);
		driver.findElement(By.xpath(Export_xpath)).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath(Excel_export)).click();
		Thread.sleep(10000);
	}

	@After
	public void tearDown() throws Exception {
		 driver.quit();
	}

}
