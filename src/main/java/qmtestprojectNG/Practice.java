package qmtestprojectNG;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.Date;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import javax.mail.*;
import javax.mail.internet.*;

public class Practice {

	public String qTest_url, WITS2ndPass_url, iphone_url, ipad_url, android_url, webapp_url;
	public String totalTestRun, totalTestUnexecuted, totalTestExecuted, totalTestPassed, totalTestFailed, totalTestIncomplete;
	public String iphoneTestRun, iphoneTestUnexecuted, iphoneTestExecuted, iphoneTestPassed, iphoneTestFailed, iphoneTestIncomplete;
	public String ipadTestRun, ipadTestUnexecuted, ipadTestExecuted, ipadTestPassed, ipadTestFailed, ipadTestIncomplete;
	public String androidTestRun, androidTestUnexecuted, androidTestExecuted,androidTestPassed, androidTestFailed, androidTestIncomplete;
	public String webappTestRun, webappTestUnexecuted, webappTestExecuted, webappTestPassed, webappTestFailed, webappTestIncomplete;
	public String myProfileLogout = ".//*[@id='avatarHeader']", myProfileLogout2 = ".//*[@id='log-out-link']";
	public WebDriver driver = new FirefoxDriver();
//	public WebElement testRunWE, testUnexecutedWE, testExecutedWE, testPassedWE, testFailedWE, testIncompleteWE;
	
	@Before
	public void setUp() throws Exception {
		qTest_url = "https://quickmobile.qtestnet.com"; // qTest
		WITS2ndPass_url = "https://goo.gl/ML9N1Y"; // WITS-2nd Pass URL
		iphone_url = "https://goo.gl/N5cQQQ"; // iPhone URL
		ipad_url = "https://goo.gl/14rPBC"; // iPad URL
		android_url = "https://goo.gl/u1NUUN"; // Android URL
		webapp_url = "https://goo.gl/neHRJW"; // WebApp URL
		driver.manage().timeouts().implicitlyWait(1000, TimeUnit.SECONDS);
	}

	@Test
	public void test() throws InterruptedException, AddressException, MessagingException {
		driver.get(qTest_url);
		Login();
		Calculator(); 
	}

	public void Login() throws InterruptedException {
		driver.findElement(By.xpath("//*[@id='userName']")).clear();
		driver.findElement(By.xpath("//*[@id='userName']")).click();
		driver.findElement(By.xpath("//*[@id='userName']")).sendKeys("Ting.Yen@quickmobile.com");
		driver.findElement(By.xpath("//*[@id='passwordPlaceholder']")).click();
		driver.findElement(By.xpath("//*[@id='password']")).clear();
		driver.findElement(By.xpath("//*[@id='password']")).sendKeys("WITS*8888");
		Thread.sleep(2000);
		driver.findElement(By.xpath(".//*[@id='loginForm']/div/div[1]/a/img")).click();
	}

	/*
	 	public String TotalTestRun, TotalTestUnexecuted, TotalTestExecuted, TotalTestPassed, TotalTestFailed, TotalTestIncomplete;
		public String iphoneTestRun, iphoneTestUnexecuted, iphoneTestExecuted, iphoneTestPassed, iphoneTestFailed, iphoneTestIncomplete;
		public String ipadTestRun, ipadTestUnexecuted, ipadTestExecuted, ipadTestPassed, ipadTestFailed, ipadTestIncomplete;
		public String androidTestRun, androidTestUnexecuted, androidTestExecuted,androidTestPassed, androidTestFailed, androidTestIncomplete;
		public String webappTestRun, webappTestUnexecute, webappTestExecuted, webappTestPassed, webappTestFailed, webappTestIncomplete;
	 */
	
	public void Calculator() throws AddressException, MessagingException, InterruptedException {
		driver.get(WITS2ndPass_url);
		
//		testRunWE = driver.findElement(By.xpath(".//*[@id='sumTestRunTotal']"));
//		testUnexecutedWE = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[5]/div/div[2]/span"));
//		testExecutedWE = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[1]/div/div[2]/span"));
//		testPassedWE = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[2]/div/div[2]/span"));
//		testFailedWE = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[3]/div/div[2]/span"));
//		testIncompleteWE = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[4]/div/div[2]/span"));
		
		totalTestRun = driver.findElement(By.xpath(".//*[@id='sumTestRunTotal']")).getText();
		totalTestUnexecuted = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[5]/div/div[2]/span")).getText();
		totalTestExecuted = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[1]/div/div[2]/span")).getText();
		totalTestPassed = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[2]/div/div[2]/span")).getText();
		totalTestFailed = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[3]/div/div[2]/span")).getText();
		totalTestIncomplete = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[4]/div/div[2]/span")).getText();
		
		driver.get(iphone_url);
		Thread.sleep(8000);
		iphoneTestRun = driver.findElement(By.xpath(".//*[@id='sumTestRunTotal']")).getText();
		iphoneTestUnexecuted = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[5]/div/div[2]/span")).getText();
		iphoneTestExecuted = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[1]/div/div[2]/span")).getText();
		iphoneTestPassed = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[2]/div/div[2]/span")).getText();
		iphoneTestFailed = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[3]/div/div[2]/span")).getText();
		iphoneTestIncomplete = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[4]/div/div[2]/span")).getText();
		
		driver.get(ipad_url);
		Thread.sleep(8000);
		ipadTestRun = driver.findElement(By.xpath(".//*[@id='sumTestRunTotal']")).getText();
		ipadTestUnexecuted = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[5]/div/div[2]/span")).getText();
		ipadTestExecuted = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[1]/div/div[2]/span")).getText();
		ipadTestPassed = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[2]/div/div[2]/span")).getText();
		ipadTestFailed = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[3]/div/div[2]/span")).getText();
		ipadTestIncomplete = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[4]/div/div[2]/span")).getText();
		
		driver.get(android_url);
		Thread.sleep(8000);
		androidTestRun = driver.findElement(By.xpath(".//*[@id='sumTestRunTotal']")).getText();
		androidTestUnexecuted = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[5]/div/div[2]/span")).getText();
		androidTestExecuted = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[1]/div/div[2]/span")).getText();
		androidTestPassed = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[2]/div/div[2]/span")).getText();
		androidTestFailed = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[3]/div/div[2]/span")).getText();
		androidTestIncomplete = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[4]/div/div[2]/span")).getText();
		
		driver.get(webapp_url);
		Thread.sleep(8000);
		webappTestRun = driver.findElement(By.xpath(".//*[@id='sumTestRunTotal']")).getText();
		webappTestUnexecuted = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[5]/div/div[2]/span")).getText();
		webappTestExecuted = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[1]/div/div[2]/span")).getText();
		webappTestPassed = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[2]/div/div[2]/span")).getText();
		webappTestFailed = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[3]/div/div[2]/span")).getText();
		webappTestIncomplete = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[4]/div/div[2]/span")).getText();
		
		SendMail();

	}

	public void SendMail() throws AddressException, MessagingException {
		try{
		String host = "tpma2001.wistronits.com";
		String from = "JakeChen@wistronits.com";
		String to = "JakeChen@wistronits.com";
		long time = System.currentTimeMillis();
		Date date = new Date(time);
		Properties props = System.getProperties();
		props.put("mail.smtp.host", host);
		Session mailSession = Session.getInstance(props, null);
		Message mailMessage = new MimeMessage(mailSession);
		mailMessage.setFrom(new InternetAddress(from));
		mailMessage.setRecipient(Message.RecipientType.TO, new InternetAddress(to));
		mailMessage.setSentDate(date);
		mailMessage.setSubject("QuickMobile->3.5->Drive to zero");
//		mailMessage.setText("Hi all,\n\nThis is a test mail, please ignore this mail.\n\nJake.");
		StringBuffer sb = new StringBuffer();
		sb.append("QuickMobile->3.5->Drive to zero :\n\n\n");
		sb.append("<style>table{border-collapse: collapse;width: 100%;}th{text-align: right;padding: 15px;}td {text-align: right;padding: 15px;}tr:nth-child(even){background-color: #f2f2f2}th {background-color: #4CAF50;color: black;}</style>");
		sb.append("<table border=\"1\">");
		sb.append("<tr><th></th><th>Total Test Runs</th><th>Total Test Executed</th><th>Unexecuted</th><th>Passed</th><th>Failed</th><th>Incomplete</th></tr>");
		sb.append("<tr><td>Total</td><td>"+totalTestRun+"</td><td>"+totalTestExecuted+"</td><td>"+totalTestUnexecuted+"</td><td>"+totalTestPassed+"</td><td>"+totalTestFailed+"</td><td>"+totalTestIncomplete+"</td></tr>");
		sb.append("<tr><td>iPhone</td><td>"+iphoneTestRun+"</td><td>"+iphoneTestExecuted+"</td><td>"+iphoneTestUnexecuted+"</td><td>"+iphoneTestPassed+"</td><td>"+iphoneTestFailed+"</td><td>"+iphoneTestIncomplete+"</td></tr>");
		sb.append("<tr><td>iPad</td><td>"+ipadTestRun+"</td><td>"+ipadTestExecuted+"</td><td>"+ipadTestUnexecuted+"</td><td>"+ipadTestPassed+"</td><td>"+ipadTestFailed+"</td><td>"+ipadTestIncomplete+"</td></tr>");
		sb.append("<tr><td>Android</td><td>"+androidTestRun+"</td><td>"+androidTestExecuted+"</td><td>"+androidTestUnexecuted+"</td><td>"+androidTestPassed+"</td><td>"+androidTestFailed+"</td><td>"+androidTestIncomplete+"</td></tr>");
		sb.append("<tr><td>WebApp</td><td>"+webappTestRun+"</td><td>"+webappTestExecuted+"</td><td>"+webappTestUnexecuted+"</td><td>"+webappTestPassed+"</td><td>"+webappTestFailed+"</td><td>"+webappTestIncomplete+"</td></tr>");
		sb.append("</table>");
		mailMessage.setContent(sb.toString(), "text/html");
		Transport.send(mailMessage);
		System.out.println("\n Mail was sent successfully.");
		}
		catch (Exception e){
			System.out.println(e.getMessage());
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.findElement(By.xpath(myProfileLogout)).click();
		driver.findElement(By.xpath(myProfileLogout2)).click();
		driver.quit();
	}

}
