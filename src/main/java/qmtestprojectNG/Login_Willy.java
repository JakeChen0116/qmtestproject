package qmtestprojectNG;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.remote.MobilePlatform;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Login_Willy {
	AndroidDriver driver;
	Dimension size;

	@Test
	public void f() throws InterruptedException {

		WebDriverWait wait = new WebDriverWait(driver, 3);
		wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.ImageButton[@content-desc='Ok']")));

		driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc='Ok']")).click();

		size = driver.manage().window().getSize();
		int starty = (int) (size.height * 0.80);
		int endy = (int) (size.height * 0.20);
		int startx = size.width / 2;

		for (int second = 0; second <= 60; second++) {
			// List<WebElement> list =
			// driver.findElements(By.xpath("//android.widget.TextView[@text='My
			// Profile']"));
			// Boolean isPresent =
			// driver.findElements(By.xpath("//android.widget.TextView[@text='My
			// Profile']")).isEmpty();
			if (isElementPresent(By.xpath("//android.widget.TextView[@text='My Profile']")))
				break;
			else {
				driver.swipe(startx, starty, startx, endy, 3000);
				Thread.sleep(1000);
			}
		}
		driver.findElement(By.xpath("//android.widget.TextView[@text='My Profile']")).click();

	}

	public boolean isElementPresent(By by) {
		driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
		List<WebElement> list = driver.findElements(by);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		if (list.size() == 0) {
			return false;
		} else {
			// return list.get(0).isDisplayed();
			return true;
		}
	}

	// An implicit wait is to tell WebDriver to poll the DOM for a certain
	// amount of time when
	// trying to find an element or elements if they are not immediately
	// available. The default
	// setting is 0. Once set, the implicit wait is set for the life of the
	// WebDriver object instance.

	@BeforeClass
	public void beforeClass() throws MalformedURLException {

		DesiredCapabilities caps = new DesiredCapabilities();
		caps.setCapability(MobileCapabilityType.PLATFORM_NAME, MobilePlatform.ANDROID);
		caps.setCapability(MobileCapabilityType.DEVICE_NAME, "Android device");
		caps.setCapability(MobileCapabilityType.APP_PACKAGE, "com.quickmobile.pushtesting");
		caps.setCapability(MobileCapabilityType.APP_ACTIVITY,
				"com.quickmobile.conference.ApplicationInitialLoadActivity");
		driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), caps);
		driver.manage().timeouts().implicitlyWait(1000, TimeUnit.SECONDS);

	}

	@AfterClass
	public void afterClass() {
	}

}
