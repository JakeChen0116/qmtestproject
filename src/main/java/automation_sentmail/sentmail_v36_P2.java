package automation_sentmail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import java.util.Date;
import java.util.Properties;
import javax.mail.*;
import javax.mail.internet.*;

public class sentmail_v36_P2 {

	public int i = 0;
	public boolean bool;
	public String browser;
	
	public String qTest_url, WITS2ndPass_url, iphone_url, ipad_url, android_url, webapp_url;
	public String iphoneM1_url, ipadM1_url, androidM1_url, webappM1_url;
	public String iphoneM2_url, ipadM2_url, androidM2_url, webappM2_url;
	
	public String totalTestRun, totalTestUnexecuted, totalTestExecuted, totalTestPassed, totalTestFailed, totalTestIncomplete;
	public String iphoneTestRun, iphoneTestUnexecuted, iphoneTestExecuted, iphoneTestPassed, iphoneTestFailed, iphoneTestIncomplete;
	public String ipadTestRun, ipadTestUnexecuted, ipadTestExecuted, ipadTestPassed, ipadTestFailed, ipadTestIncomplete;
	public String androidTestRun, androidTestUnexecuted, androidTestExecuted,androidTestPassed, androidTestFailed, androidTestIncomplete;
	public String webappTestRun, webappTestUnexecuted, webappTestExecuted, webappTestPassed, webappTestFailed, webappTestIncomplete;
	
	public int totalTestRunM1, totalTestUnexecutedM1, totalTestExecutedM1, totalTestPassedM1, totalTestFailedM1, totalTestIncompleteM1;
	public String iphoneTestRunM1, iphoneTestUnexecutedM1, iphoneTestExecutedM1, iphoneTestPassedM1, iphoneTestFailedM1, iphoneTestIncompleteM1;
	public String ipadTestRunM1, ipadTestUnexecutedM1, ipadTestExecutedM1, ipadTestPassedM1, ipadTestFailedM1, ipadTestIncompleteM1;
	public String androidTestRunM1, androidTestUnexecutedM1, androidTestExecutedM1,androidTestPassedM1, androidTestFailedM1, androidTestIncompleteM1;
	public String webappTestRunM1, webappTestUnexecutedM1, webappTestExecutedM1, webappTestPassedM1, webappTestFailedM1, webappTestIncompleteM1;
	
	public int totalTestRunM2, totalTestUnexecutedM2, totalTestExecutedM2, totalTestPassedM2, totalTestFailedM2, totalTestIncompleteM2;
	public String iphoneTestRunM2, iphoneTestUnexecutedM2, iphoneTestExecutedM2, iphoneTestPassedM2, iphoneTestFailedM2, iphoneTestIncompleteM2;
	public String ipadTestRunM2, ipadTestUnexecutedM2, ipadTestExecutedM2, ipadTestPassedM2, ipadTestFailedM2, ipadTestIncompleteM2;
	public String androidTestRunM2, androidTestUnexecutedM2, androidTestExecutedM2,androidTestPassedM2, androidTestFailedM2, androidTestIncompleteM2;
	public String webappTestRunM2, webappTestUnexecutedM2, webappTestExecutedM2, webappTestPassedM2, webappTestFailedM2, webappTestIncompleteM2;
	
	public String myProfileLogout = ".//*[@id='avatarHeader']", myProfileLogout2 = ".//*[@id='log-out-link']";
	public WebDriver driver = new FirefoxDriver();
	public String zero = "0";
	
	public String url = "https://quickmobile.qtestnet.com/p/4372/portal/project#tab=testexecution&object=4&id=";
	public int number = 0;

	@Before
	public void setUp() throws Exception {
        qTest_url = "https://quickmobile.qtestnet.com" ; // qTest
        WITS2ndPass_url = "https://goo.gl/U4LYov" ; // WITS Pass URL
        
        number = 238325;
        iphone_url = url + Integer.toString(number); // iPhone URL
        iphoneM1_url =url + Integer.toString(number+1); // iPhonem1 URL
       // iphoneM2_url =  url + Integer.toString(number+2); // iPhonem2 URL
        ipad_url = url + Integer.toString(number+3); // iPad URL
        ipadM1_url = url + Integer.toString(number+4); // iPadm1 URL
       // ipadM2_url = url + Integer.toString(number+5); // iPadm2 URL
        android_url =url + Integer.toString(number+6); // Android URL
        androidM1_url = url + Integer.toString(number+7); // Androidm1 URL
        //androidM2_url = url + Integer.toString(number+8); // Androidm2 URL
        webapp_url = url + Integer.toString(number+9); // WebApp URL
        webappM1_url = url + Integer.toString(number+10); // WebAppm1 URL
        //webappM2_url =url + Integer.toString(number+11); // WebAppm2 URL
        
        System.out.println(iphone_url);
        System.out.println(iphoneM1_url);
       // System.out.println(iphoneM2_url);
        System.out.println(ipad_url);
        System.out.println(ipadM1_url);
        //System.out.println(ipadM2_url);
        System.out.println(android_url);
        System.out.println(androidM1_url);
       //System.out.println(androidM2_url);       
        System.out.println(webapp_url);
        System.out.println(webappM1_url);
        //System.out.println(webappM2_url);
	}

	@Test
	public void test() throws Exception {
		driver.get(qTest_url);
		Login();
		execute();
		SendMail();
	}
	
	public void execute() throws AddressException, MessagingException, InterruptedException{
		Calculator(); 
		CalculatorM1();
		//CalculatorM2();
	}

	public void Login() throws Exception {
		driver.findElement(By.xpath("//*[@id='userName']")).sendKeys("Ting.Yen@quickmobile.com");
		driver.findElement(By.xpath("//*[@id='passwordPlaceholder']")).click();
		driver.findElement(By.xpath("//*[@id='password']")).sendKeys("WITS*8888");
		driver.findElement(By.xpath(".//*[@id='loginForm']/div/div[1]/a/img")).click();

		bool = isElementPresent(By.xpath("//*[@id='activeSessionDialog']/div/div/div[1]/h4"));

		if (bool == false) {
			Thread.sleep(4000);
			Calculator();
			Thread.sleep(4000);
			CalculatorM1();
			Thread.sleep(4000);
			//CalculatorM2();
			//Thread.sleep(4000);
			SendMail();
			Thread.sleep(1000);
			tearDown();
		}

		Thread.sleep(4000);
		for (i = 1; i < 4; i++) {
			WebElement element = driver
					.findElement(By.xpath("//*[@id='activeSessionTable']/tbody/tr[" + i + "]/td[3]"));
			Thread.sleep(4000);
			browser = element.getText().toString();
			// System.out.println(browser);
			switch (browser) {

			case "Edge":
				cal();
				Thread.sleep(4000);
				execute();
				Thread.sleep(4000);
				SendMail();
				Thread.sleep(4000);
				tearDown();
				break;

			case "Firefox":
				cal();
				Thread.sleep(4000);
				execute();
				Thread.sleep(4000);
				SendMail();
				Thread.sleep(4000);
				tearDown();
				break;

			case "Chrome":
				cal();
				Thread.sleep(4000);
				execute();
				Thread.sleep(4000);
				SendMail();
				Thread.sleep(4000);
				tearDown();
				break;
			}
		}
		i--;
		cal();
		Thread.sleep(4000);
		execute();
		Thread.sleep(4000);
		SendMail();
		Thread.sleep(4000);
		tearDown();
	}

	public void cal() throws Exception {
		Thread.sleep(1000);
		driver.findElement(By.xpath(".//*[@id='activeSessionTable']/tbody/tr[" + i + "]/td[8]/a/span")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath(".//*[@id='reloginBtn']")).click();
	}
	
	public void Calculator() throws AddressException, MessagingException, InterruptedException {
		driver.get(WITS2ndPass_url);
		Thread.sleep(8000);
		totalTestRun = driver.findElement(By.xpath(".//*[@id='sumTestRunTotal']")).getText();
		//**************************************************************************************//
		if(isElementPresent(By.xpath(".//*[@id='sumTestRun']/tr/td[1]/div/div[2]/span")) == true){
			totalTestExecuted = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[1]/div/div[2]/span")).getText();
		}
		else{
			totalTestExecuted = zero;
		}
		if(isElementPresent(By.xpath(".//*[@id='sumTestRun']/tr/td[2]/div/div[2]/span")) == true){
			totalTestPassed = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[2]/div/div[2]/span")).getText();
		}
		else{
			totalTestPassed = zero;
		}
		if(isElementPresent(By.xpath(".//*[@id='sumTestRun']/tr/td[3]/div/div[2]/span")) == true){
			totalTestFailed = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[3]/div/div[2]/span")).getText();
		}
		else{
			totalTestFailed = zero;
		}
		if(isElementPresent(By.xpath(".//*[@id='sumTestRun']/tr/td[4]/div/div[2]/span")) == true){
			totalTestIncomplete = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[4]/div/div[2]/span")).getText();
		}
		else{
			totalTestIncomplete = zero;
		}
		if(isElementPresent(By.xpath(".//*[@id='sumTestRun']/tr/td[5]/div/div[2]/span")) == true){
			totalTestUnexecuted = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[5]/div/div[2]/span")).getText();
		}
		else{
			totalTestUnexecuted = zero;
		}

		driver.get(iphone_url);
		Thread.sleep(8000);
		iphoneTestRun = driver.findElement(By.xpath(".//*[@id='sumTestRunTotal']")).getText();
		//**************************************************************************************//
		if(isElementPresent(By.xpath(".//*[@id='sumTestRun']/tr/td[1]/div/div[2]/span")) == true){
			iphoneTestExecuted = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[1]/div/div[2]/span")).getText();
		}
		else{
			iphoneTestExecuted = zero;
		}
		iphoneTestRun = driver.findElement(By.xpath(".//*[@id='sumTestRunTotal']")).getText();
		if(isElementPresent(By.xpath(".//*[@id='sumTestRun']/tr/td[2]/div/div[2]/span")) == true){
			iphoneTestPassed = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[2]/div/div[2]/span")).getText();
		}
		else{
			iphoneTestPassed = zero;
		}
		iphoneTestRun = driver.findElement(By.xpath(".//*[@id='sumTestRunTotal']")).getText();
		if(isElementPresent(By.xpath(".//*[@id='sumTestRun']/tr/td[3]/div/div[2]/span")) == true){
			iphoneTestFailed = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[3]/div/div[2]/span")).getText();
		}
		else{
			iphoneTestFailed = zero;
		}
		iphoneTestRun = driver.findElement(By.xpath(".//*[@id='sumTestRunTotal']")).getText();
		if(isElementPresent(By.xpath(".//*[@id='sumTestRun']/tr/td[4]/div/div[2]/span")) == true){
			iphoneTestIncomplete = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[4]/div/div[2]/span")).getText();
		}
		else{
			iphoneTestIncomplete = zero;
		}
		if(isElementPresent(By.xpath(".//*[@id='sumTestRun']/tr/td[5]/div/div[2]/span")) == true){
			iphoneTestUnexecuted = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[5]/div/div[2]/span")).getText();
		}
		else{
			iphoneTestUnexecuted = zero;
		}
		
		driver.get(ipad_url);
		Thread.sleep(8000);
		ipadTestRun = driver.findElement(By.xpath(".//*[@id='sumTestRunTotal']")).getText();
		//**************************************************************************************//
		if(isElementPresent(By.xpath(".//*[@id='sumTestRun']/tr/td[1]/div/div[2]/span")) == true){
			ipadTestExecuted = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[1]/div/div[2]/span")).getText();
		}
		else{
			ipadTestExecuted = zero;
		}
		if(isElementPresent(By.xpath(".//*[@id='sumTestRun']/tr/td[2]/div/div[2]/span")) == true){
			ipadTestPassed = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[2]/div/div[2]/span")).getText();
		}
		else{
			ipadTestPassed = zero;
		}
		if(isElementPresent(By.xpath(".//*[@id='sumTestRun']/tr/td[3]/div/div[2]/span")) == true){
			ipadTestFailed = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[3]/div/div[2]/span")).getText();
		}
		else{
			ipadTestFailed = zero;
		}
		if(isElementPresent(By.xpath(".//*[@id='sumTestRun']/tr/td[4]/div/div[2]/span")) == true){
			ipadTestIncomplete = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[4]/div/div[2]/span")).getText();
		}
		else{
			ipadTestIncomplete = zero;
		}
		if(isElementPresent(By.xpath(".//*[@id='sumTestRun']/tr/td[5]/div/div[2]/span")) == true){
			ipadTestUnexecuted = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[5]/div/div[2]/span")).getText();
		}
		else{
			ipadTestUnexecuted = zero;
		}
		
		driver.get(android_url);
		Thread.sleep(8000);
		androidTestRun = driver.findElement(By.xpath(".//*[@id='sumTestRunTotal']")).getText();
		//**************************************************************************************//
		if(isElementPresent(By.xpath(".//*[@id='sumTestRun']/tr/td[1]/div/div[2]/span")) == true){
			androidTestExecuted = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[1]/div/div[2]/span")).getText();
		}
		else{
			androidTestExecuted = zero;
		}
		if(isElementPresent(By.xpath(".//*[@id='sumTestRun']/tr/td[2]/div/div[2]/span")) == true){
			androidTestPassed = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[2]/div/div[2]/span")).getText();
		}
		else{
			androidTestPassed = zero;
		}
		if(isElementPresent(By.xpath(".//*[@id='sumTestRun']/tr/td[3]/div/div[2]/span")) == true){
			androidTestFailed = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[3]/div/div[2]/span")).getText();
		}
		else{
			androidTestFailed = zero;
		}
		if(isElementPresent(By.xpath(".//*[@id='sumTestRun']/tr/td[4]/div/div[2]/span")) == true){
			androidTestIncomplete = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[4]/div/div[2]/span")).getText();
		}
		else{
			androidTestIncomplete = zero;
		}
		if(isElementPresent(By.xpath(".//*[@id='sumTestRun']/tr/td[5]/div/div[2]/span")) == true){
			androidTestUnexecuted = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[5]/div/div[2]/span")).getText();
		}
		else{
			androidTestUnexecuted = zero;
		}

		driver.get(webapp_url);
		Thread.sleep(8000);
		webappTestRun = driver.findElement(By.xpath(".//*[@id='sumTestRunTotal']")).getText();
		//**************************************************************************************//
		if(isElementPresent(By.xpath(".//*[@id='sumTestRun']/tr/td[1]/div/div[2]/span")) == true){
			webappTestExecuted = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[1]/div/div[2]/span")).getText();
		}
		else{
			webappTestExecuted = zero;
		}
		if(isElementPresent(By.xpath(".//*[@id='sumTestRun']/tr/td[2]/div/div[2]/span")) == true){
			webappTestPassed = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[2]/div/div[2]/span")).getText();
		}
		else{
			webappTestPassed = zero;
		}
		if(isElementPresent(By.xpath(".//*[@id='sumTestRun']/tr/td[3]/div/div[2]/span")) == true){
			webappTestFailed = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[3]/div/div[2]/span")).getText();
		}
		else{
			webappTestFailed = zero;
		}
		if(isElementPresent(By.xpath(".//*[@id='sumTestRun']/tr/td[4]/div/div[2]/span")) == true){
			webappTestIncomplete = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[4]/div/div[2]/span")).getText();
		}
		else{
			webappTestIncomplete = zero;
		}
		if(isElementPresent(By.xpath(".//*[@id='sumTestRun']/tr/td[5]/div/div[2]/span")) == true){
			webappTestUnexecuted = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[5]/div/div[2]/span")).getText();
		}
		else{
			webappTestUnexecuted = zero;
		}
	}

	public void CalculatorM1() throws AddressException, MessagingException, InterruptedException{
		driver.get(iphoneM1_url);
		Thread.sleep(8000);
		iphoneTestRunM1 = driver.findElement(By.xpath(".//*[@id='sumTestRunTotal']")).getText();
		//**************************************************************************************//
		if(isElementPresent(By.xpath(".//*[@id='sumTestRun']/tr/td[1]/div/div[2]/span")) == true){
			iphoneTestExecutedM1 = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[1]/div/div[2]/span")).getText();
		}
		else{
			iphoneTestExecutedM1 = zero;
		}
		if(isElementPresent(By.xpath(".//*[@id='sumTestRun']/tr/td[2]/div/div[2]/span")) == true){
			iphoneTestPassedM1 = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[2]/div/div[2]/span")).getText();
		}
		else{
			iphoneTestPassedM1 = zero;
		}
		if(isElementPresent(By.xpath(".//*[@id='sumTestRun']/tr/td[4]/div/div[2]/span")) == true){
			iphoneTestFailedM1 = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[4]/div/div[2]/span")).getText();
		}
		else{
			iphoneTestFailedM1 = zero;
		}
		if(isElementPresent(By.xpath(".//*[@id='sumTestRun']/tr/td[3]/div/div[2]/span")) == true){
			iphoneTestIncompleteM1 = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[3]/div/div[2]/span")).getText();
		}
		else{
			iphoneTestIncompleteM1 = zero;
		}
		if(isElementPresent(By.xpath(".//*[@id='sumTestRun']/tr/td[5]/div/div[2]/span")) == true){
			iphoneTestUnexecutedM1 = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[5]/div/div[2]/span")).getText();
		}
		else{
			iphoneTestUnexecutedM1 = zero;
		}

		driver.get(ipadM1_url);
		Thread.sleep(8000);
		ipadTestRunM1 = driver.findElement(By.xpath(".//*[@id='sumTestRunTotal']")).getText();		
		//*************************************************************************************************************//
		if(isElementPresent(By.xpath(".//*[@id='sumTestRun']/tr/td[1]/div/div[2]/span")) == true){
			ipadTestExecutedM1 = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[1]/div/div[2]/span")).getText();
		}
		else{
			ipadTestExecutedM1 = zero;
		}
		if(isElementPresent(By.xpath(".//*[@id='sumTestRun']/tr/td[2]/div/div[2]/span")) == true){
			ipadTestPassedM1 = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[2]/div/div[2]/span")).getText();
		}
		else{
			ipadTestPassedM1 = zero;
		}
		if(isElementPresent(By.xpath(".//*[@id='sumTestRun']/tr/td[4]/div/div[2]/span")) == true){
			ipadTestFailedM1 = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[4]/div/div[2]/span")).getText();
		}
		else{
			ipadTestFailedM1 = zero;
		}
		if(isElementPresent(By.xpath(".//*[@id='sumTestRun']/tr/td[3]/div/div[2]/span")) == true){
			ipadTestIncompleteM1 = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[3]/div/div[2]/span")).getText();
		}
		else{
			ipadTestIncompleteM1 = zero;
		}
		if(isElementPresent(By.xpath(".//*[@id='sumTestRun']/tr/td[5]/div/div[2]/span")) == true){
			ipadTestUnexecutedM1 = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[5]/div/div[2]/span")).getText();
		}
		else{
			ipadTestUnexecutedM1 = zero;
		}
		
		driver.get(androidM1_url);
		Thread.sleep(8000);
		androidTestRunM1 = driver.findElement(By.xpath(".//*[@id='sumTestRunTotal']")).getText();
		//*************************************************************************************************************//
		if(isElementPresent(By.xpath(".//*[@id='sumTestRun']/tr/td[1]/div/div[2]/span")) == true){
			androidTestExecutedM1 = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[1]/div/div[2]/span")).getText();
		}
		else{
			androidTestExecutedM1 = zero;
		}
		if(isElementPresent(By.xpath(".//*[@id='sumTestRun']/tr/td[2]/div/div[2]/span")) == true){
			androidTestPassedM1 = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[2]/div/div[2]/span")).getText();
		}
		else{
			androidTestPassedM1 = zero;
		}
		if(isElementPresent(By.xpath(".//*[@id='sumTestRun']/tr/td[3]/div/div[2]/span")) == true){
			androidTestFailedM1 = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[3]/div/div[2]/span")).getText();
		}
		else{
			androidTestFailedM1 = zero;
		}
		if(isElementPresent(By.xpath(".//*[@id='sumTestRun']/tr/td[4]/div/div[2]/span")) == true){
			androidTestIncompleteM1 = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[4]/div/div[2]/span")).getText();
		}
		else{
			androidTestIncompleteM1 = zero;
		}
		if(isElementPresent(By.xpath(".//*[@id='sumTestRun']/tr/td[5]/div/div[2]/span")) == true){
			androidTestUnexecutedM1 = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[5]/div/div[2]/span")).getText();
		}
		else{
			androidTestUnexecutedM1 = zero;
		}
		
		driver.get(webappM1_url);
		Thread.sleep(8000);
		webappTestRunM1 = driver.findElement(By.xpath(".//*[@id='sumTestRunTotal']")).getText();
		//*************************************************************************************************************//
		if(isElementPresent(By.xpath(".//*[@id='sumTestRun']/tr/td[1]/div/div[2]/span")) == true){
			webappTestExecutedM1 = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[1]/div/div[2]/span")).getText();
		}
		else{
			webappTestExecutedM1 = zero;
		}
		if(isElementPresent(By.xpath(".//*[@id='sumTestRun']/tr/td[2]/div/div[2]/span")) == true){
			webappTestPassedM1 = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[2]/div/div[2]/span")).getText();
		}
		else{
			webappTestPassedM1 = zero;
		}
		if(isElementPresent(By.xpath(".//*[@id='sumTestRun']/tr/td[3]/div/div[2]/span")) == true){
			webappTestFailedM1 = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[3]/div/div[2]/span")).getText();
		}
		else{
			webappTestFailedM1 = zero;
		}
		if(isElementPresent(By.xpath(".//*[@id='sumTestRun']/tr/td[4]/div/div[2]/span")) == true){
			webappTestIncompleteM1 = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[4]/div/div[2]/span")).getText();
		}
		else{
			webappTestIncompleteM1 = zero;
		}
		if(isElementPresent(By.xpath(".//*[@id='sumTestRun']/tr/td[5]/div/div[2]/span")) == true){
			webappTestUnexecutedM1 = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[5]/div/div[2]/span")).getText();
		}
		else{
			webappTestUnexecutedM1 = zero;
		}
		
		Thread.sleep(2000);
		totalTestRunM1 = Integer.parseInt(iphoneTestRunM1) + Integer.parseInt(ipadTestRunM1) + Integer.parseInt(androidTestRunM1) + Integer.parseInt(webappTestRunM1);
		totalTestUnexecutedM1 = Integer.parseInt(iphoneTestUnexecutedM1) + Integer.parseInt(ipadTestUnexecutedM1) + Integer.parseInt(androidTestUnexecutedM1) + Integer.parseInt(webappTestUnexecutedM1);
		totalTestExecutedM1 = Integer.parseInt(iphoneTestExecutedM1) + Integer.parseInt(ipadTestExecutedM1) + Integer.parseInt(androidTestExecutedM1) + Integer.parseInt(webappTestExecutedM1); 
		totalTestPassedM1 = Integer.parseInt(iphoneTestPassedM1) + Integer.parseInt(ipadTestPassedM1) + Integer.parseInt(androidTestPassedM1) + Integer.parseInt(webappTestPassedM1);
		totalTestFailedM1 = Integer.parseInt(iphoneTestFailedM1) + Integer.parseInt(ipadTestFailedM1) + Integer.parseInt(androidTestFailedM1) + Integer.parseInt(webappTestFailedM1);
		totalTestIncompleteM1 = Integer.parseInt(iphoneTestIncompleteM1) + Integer.parseInt(ipadTestIncompleteM1) + Integer.parseInt(androidTestIncompleteM1) + Integer.parseInt(webappTestIncompleteM1);
	}
	
	public void CalculatorM2() throws AddressException, MessagingException, InterruptedException{
		driver.get(iphoneM2_url);
		Thread.sleep(8000);
		iphoneTestRunM2 = driver.findElement(By.xpath(".//*[@id='sumTestRunTotal']")).getText();
		if(isElementPresent(By.xpath(".//*[@id='sumTestRun']/tr/td[5]/div/div[2]/span")) == true){
			iphoneTestUnexecutedM2 = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[5]/div/div[2]/span")).getText();
		}
		else{
			iphoneTestUnexecutedM2 = zero;
		}
//		iphoneTestUnexecutedM2 = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[5]/div/div[2]/span")).getText();
		iphoneTestExecutedM2 = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[1]/div/div[2]/span")).getText();
		iphoneTestPassedM2 = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[2]/div/div[2]/span")).getText();
		iphoneTestFailedM2 = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[3]/div/div[2]/span")).getText();
		iphoneTestIncompleteM2 = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[4]/div/div[2]/span")).getText();
		
		driver.get(ipadM2_url);
		Thread.sleep(8000);
		ipadTestRunM2 = driver.findElement(By.xpath(".//*[@id='sumTestRunTotal']")).getText();
		if(isElementPresent(By.xpath(".//*[@id='sumTestRun']/tr/td[5]/div/div[2]/span")) == true){
			ipadTestUnexecutedM2 = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[5]/div/div[2]/span")).getText();
		}
		else{
			ipadTestUnexecutedM2 = zero;
		}
//		ipadTestUnexecutedM2 = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[5]/div/div[2]/span")).getText();
		ipadTestExecutedM2 = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[1]/div/div[2]/span")).getText();
		ipadTestPassedM2 = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[2]/div/div[2]/span")).getText();
		ipadTestFailedM2 = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[3]/div/div[2]/span")).getText();
		ipadTestIncompleteM2 = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[4]/div/div[2]/span")).getText();
		
		driver.get(androidM2_url);
		Thread.sleep(8000);
		androidTestRunM2 = driver.findElement(By.xpath(".//*[@id='sumTestRunTotal']")).getText();
		if(isElementPresent(By.xpath(".//*[@id='sumTestRun']/tr/td[5]/div/div[2]/span")) == true){
			androidTestUnexecutedM2 = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[5]/div/div[2]/span")).getText();
		}
		else{
			androidTestUnexecutedM2 = zero;
		}
//		androidTestUnexecutedM2 = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[5]/div/div[2]/span")).getText();
		androidTestExecutedM2 = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[1]/div/div[2]/span")).getText();
		androidTestPassedM2 = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[2]/div/div[2]/span")).getText();
		androidTestFailedM2 = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[3]/div/div[2]/span")).getText();
		androidTestIncompleteM2 = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[4]/div/div[2]/span")).getText();
		
		driver.get(webappM2_url);
		Thread.sleep(8000);
		webappTestRunM2 = driver.findElement(By.xpath(".//*[@id='sumTestRunTotal']")).getText();
		if(isElementPresent(By.xpath(".//*[@id='sumTestRun']/tr/td[5]/div/div[2]/span")) == true){
			webappTestUnexecutedM2 = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[5]/div/div[2]/span")).getText();
		}
		else{
			webappTestUnexecutedM2 = zero;
		}
//		webappTestUnexecutedM2 = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[5]/div/div[2]/span")).getText();
		webappTestExecutedM2 = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[1]/div/div[2]/span")).getText();
		webappTestPassedM2 = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[2]/div/div[2]/span")).getText();
		webappTestFailedM2 = driver.findElement(By.xpath("//*[@id='sumTestRun']/tr/td[4]/div/div[2]/span")).getText();
		webappTestIncompleteM2 = driver.findElement(By.xpath(".//*[@id='sumTestRun']/tr/td[3]/div/div[2]/span")).getText();	
		
		Thread.sleep(2000);
		totalTestRunM2 = Integer.parseInt(iphoneTestRunM2) + Integer.parseInt(ipadTestRunM2) + Integer.parseInt(androidTestRunM2) + Integer.parseInt(webappTestRunM2);
		totalTestUnexecutedM2 = Integer.parseInt(iphoneTestUnexecutedM2) + Integer.parseInt(ipadTestUnexecutedM2) + Integer.parseInt(androidTestUnexecutedM2) + Integer.parseInt(webappTestUnexecutedM2);
		totalTestExecutedM2 = Integer.parseInt(iphoneTestExecutedM2) + Integer.parseInt(ipadTestExecutedM2) + Integer.parseInt(androidTestExecutedM2) + Integer.parseInt(webappTestExecutedM2); 
		totalTestPassedM2 = Integer.parseInt(iphoneTestPassedM2) + Integer.parseInt(ipadTestPassedM2) + Integer.parseInt(androidTestPassedM2) + Integer.parseInt(webappTestPassedM2);
		totalTestFailedM2 = Integer.parseInt(iphoneTestFailedM2) + Integer.parseInt(ipadTestFailedM2) + Integer.parseInt(androidTestFailedM2) + Integer.parseInt(webappTestFailedM2);
		totalTestIncompleteM2 = Integer.parseInt(iphoneTestIncompleteM2) + Integer.parseInt(ipadTestIncompleteM2) + Integer.parseInt(androidTestIncompleteM2) + Integer.parseInt(webappTestIncompleteM2);
	}

	public void SendMail() throws AddressException, MessagingException {
		try{
		String host = "tpma2001.wistronits.com";
		String from = "JakeChen@wistronits.com";
		String to = "QuickMobile@wistronits.com";  //QuickMobile@wistronits.com
		long time = System.currentTimeMillis();
		Date date = new Date(time);
		Properties props = System.getProperties();
		props.put("mail.smtp.host", host);
		Session mailSession = Session.getInstance(props, null);
		Message mailMessage = new MimeMessage(mailSession);
		mailMessage.setFrom(new InternetAddress(from));
		mailMessage.setRecipient(Message.RecipientType.TO, new InternetAddress(to));
		mailMessage.setSentDate(date);
		mailMessage.setSubject("QuickMobile->3.6 2nd Pass->Drive to zero");
//	mailMessage.setText("Hi all,\n\nThis is a test mail, please ignore this mail.\n\nJake.");
		StringBuffer sb = new StringBuffer();
		sb.append("<h1>Drive to zero:</h1>\n");
		sb.append("<p>Please refer to below table for the statistics of the testing items, thanks.<br><br></p>");
		sb.append("<!DOCTYPE html>");
		sb.append("<html>");
		sb.append("<head>");
		sb.append("<style>");
		sb.append("table{border-collapse: collapse;width: 70%;}");
		sb.append("th{text-align: right;padding: 12px;background-color: #74e8e9;color: black;}");
		sb.append("td{text-align: right;padding: 9px;color: black;}");
		sb.append(".totaltestrun{background-color:#e0e0e0;}");
		sb.append("</style>");
		sb.append("</head>");
		sb.append("<body>");
		/*Total*/
		sb.append("<table border=\"1\">");
		sb.append("<tr><th><span style=color:red;>ALL</span></th><th>Test Runs</th><th>Test Executed</th><th>Unexecuted</th><th>Passed</th><th>Failed</th><th>Incomplete</th></tr>");
		sb.append("<tr><td>iPhone</td><td>"+iphoneTestRun+"</td><td>"+iphoneTestExecuted+"</td><td><span style=color:red;>"+iphoneTestUnexecuted+"</span></td><td>"+iphoneTestPassed+"</td><td>"+iphoneTestFailed+"</td><td><span style=color:#ff8000;>"+iphoneTestIncomplete+"</span></td></tr>");
		sb.append("<tr><td>iPad</td><td>"+ipadTestRun+"</td><td>"+ipadTestExecuted+"</td><td><span style=color:red;>"+ipadTestUnexecuted+"</span></td><td>"+ipadTestPassed+"</td><td>"+ipadTestFailed+"</td><td><span style=color:#ff8000;>"+ipadTestIncomplete+"</span></td></tr>");
		sb.append("<tr><td>Android</td><td>"+androidTestRun+"</td><td>"+androidTestExecuted+"</td><td><span style=color:red;>"+androidTestUnexecuted+"</span></td><td>"+androidTestPassed+"</td><td>"+androidTestFailed+"</td><td><span style=color:#ff8000;>"+androidTestIncomplete+"</span></td></tr>");
		sb.append("<tr><td>WebApp</td><td>"+webappTestRun+"</td><td>"+webappTestExecuted+"</td><td><span style=color:red;>"+webappTestUnexecuted+"</span></td><td>"+webappTestPassed+"</td><td>"+webappTestFailed+"</td><td><span style=color:#ff8000;>"+webappTestIncomplete+"</span></td></tr>");		
		sb.append("<tr class=\"totaltestrun\"><td>Total</td><td>"+totalTestRun+"</td><td>"+totalTestExecuted+"</td><td>"+totalTestUnexecuted+"</td><td>"+totalTestPassed+"</td><td>"+totalTestFailed+"</td><td>"+totalTestIncomplete+"</td></tr>");
		sb.append("</table>");
		sb.append("<br>");
		/*M1*/
		sb.append("<table border=\"1\">");
		sb.append("<tr><th><span style=color:blue;>M1</span></th><th>Test Runs</th><th>Test Executed</th><th>Unexecuted</th><th>Passed</th><th>Failed</th><th>Incomplete</th></tr>");
		sb.append("<tr><td>iPhone</td><td>"+iphoneTestRunM1+"</td><td>"+iphoneTestExecutedM1+"</td><td><span style=color:red;>"+iphoneTestUnexecutedM1+"</span></td><td>"+iphoneTestPassedM1+"</td><td>"+iphoneTestFailedM1+"</td><td><span style=color:#ff8000;>"+iphoneTestIncompleteM1+"</span></td></tr>");
		sb.append("<tr><td>iPad</td><td>"+ipadTestRunM1+"</td><td>"+ipadTestExecutedM1+"</td><td><span style=color:red;>"+ipadTestUnexecutedM1+"</span></td><td>"+ipadTestPassedM1+"</td><td>"+ipadTestFailedM1+"</td><td><span style=color:#ff8000;>"+ipadTestIncompleteM1+"</span></td></tr>");
		sb.append("<tr><td>Android</td><td>"+androidTestRunM1+"</td><td>"+androidTestExecutedM1+"</td><td><span style=color:red;>"+androidTestUnexecutedM1+"</span></td><td>"+androidTestPassedM1+"</td><td>"+androidTestFailedM1+"</td><td><span style=color:#ff8000;>"+androidTestIncompleteM1+"</span></td></tr>");
		sb.append("<tr><td>Webapp</td><td>"+webappTestRunM1+"</td><td>"+webappTestExecutedM1+"</td><td><span style=color:red;>"+webappTestUnexecutedM1+"</span></td><td>"+webappTestPassedM1+"</td><td>"+webappTestFailedM1+"</td><td><span style=color:#ff8000;>"+webappTestIncompleteM1+"</span></td></tr>");	
		sb.append("<tr class=\"totaltestrun\"><td>Sub-Total</td><td>"+Integer.toString(totalTestRunM1)+"</td><td>"+Integer.toString(totalTestExecutedM1)+"</td><td>"+Integer.toString(totalTestUnexecutedM1)+"</td><td>"+Integer.toString(totalTestPassedM1)+"</td><td>"+Integer.toString(totalTestFailedM1)+"</td><td>"+Integer.toString(totalTestIncompleteM1)+"</td></tr>");	
		sb.append("</table>");
		sb.append("<br>");
		/*M2*/
//		sb.append("<table border=\"1\">");
//		sb.append("<tr><th><span style=color:blue;>M2</span></th><th>Test Runs</th><th>Test Executed</th><th>Unexecuted</th><th>Passed</th><th>Failed</th><th>Incomplete</th></tr>");
//		sb.append("<tr><td>iPhone</td><td>"+iphoneTestRunM2+"</td><td>"+iphoneTestExecutedM2+"</td><td><span style=color:red;>"+iphoneTestUnexecutedM2+"</span></td><td>"+iphoneTestPassedM2+"</td><td>"+iphoneTestFailedM2+"</td><td><span style=color:#ff8000;>"+iphoneTestIncompleteM2+"</span></td></tr>");
//		sb.append("<tr><td>iPad</td><td>"+ipadTestRunM2+"</td><td>"+ipadTestExecutedM2+"</td><td><span style=color:red;>"+ipadTestUnexecutedM2+"</span></td><td>"+ipadTestPassedM2+"</td><td>"+ipadTestFailedM2+"</td><td><span style=color:#ff8000;>"+ipadTestIncompleteM2+"</span></td></tr>");
//		sb.append("<tr><td>Android</td><td>"+androidTestRunM2+"</td><td>"+androidTestExecutedM2+"</td><td><span style=color:red;>"+androidTestUnexecutedM2+"</span></td><td>"+androidTestPassedM2+"</td><td>"+androidTestFailedM2+"</td><td><span style=color:#ff8000;>"+androidTestIncompleteM2+"</span></td></tr>");
//		sb.append("<tr><td>Webapp</td><td>"+webappTestRunM2+"</td><td>"+webappTestExecutedM2+"</td><td><span style=color:red;>"+webappTestUnexecutedM2+"</span></td><td>"+webappTestPassedM2+"</td><td>"+webappTestFailedM2+"</td><td><span style=color:#ff8000;>"+webappTestIncompleteM2+"</span></td></tr>");
//		sb.append("<tr class=\"totaltestrun\"><td>Sub-Total</td><td>"+Integer.toString(totalTestRunM2)+"</td><td>"+Integer.toString(totalTestExecutedM2)+"</td><td>"+Integer.toString(totalTestUnexecutedM2)+"</td><td>"+Integer.toString(totalTestPassedM2)+"</td><td>"+Integer.toString(totalTestFailedM2)+"</td><td>"+Integer.toString(totalTestIncompleteM2)+"</td></tr>");	
//		sb.append("</table>");
		sb.append("<p>Data Source: qTest.</p></body></html>");
		mailMessage.setContent(sb.toString(), "text/html");
		Transport.send(mailMessage);
		System.out.println("\n Mail was sent successfully.");
		}
		catch (Exception e){
			System.out.println(e.getMessage());
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.findElement(By.xpath(myProfileLogout)).click();
		driver.findElement(By.xpath(myProfileLogout2)).click();
		driver.close();
	}

	public boolean isElementPresent(By by) {
		try {
			driver.findElement(by);
			return true;

		} catch (NoSuchElementException e) {
			return false;
		}
	}

	public boolean isElementNotPresent(By by) {
		try {
			driver.findElement(by);
			return false;

		} catch (NoSuchElementException e) {
			return true;
		}
	}

	public boolean isElementClickable(By by) {
		try {
			driver.findElement(by).isEnabled();
			return true;

		} catch (NoSuchElementException e) {
			return false;
		}
	}

	public boolean isElementNotClickable(By by) {
		try {
			driver.findElement(by).isEnabled();
			return false;

		} catch (NoSuchElementException e) {
			return true;
		}
	}
}
