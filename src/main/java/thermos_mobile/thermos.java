package thermos_mobile;

import static org.junit.Assert.assertTrue;

import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.remote.MobilePlatform;

public class thermos {

	public static AndroidDriver<WebElement> driver;
	public int number = 1;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		DesiredCapabilities capabilites = new DesiredCapabilities();
		capabilites.setCapability(MobileCapabilityType.PLATFORM_NAME, MobilePlatform.ANDROID);
		capabilites.setCapability(MobileCapabilityType.DEVICE_NAME, "Nexux 5X");
		capabilites.setCapability("appPackage", "com.wits.wistronthermos");
		capabilites.setCapability("appActivity", ".SetupProfile.HelloActivity");
		driver = new AndroidDriver<WebElement>(new URL("http://127.0.0.1:4723/wd/hub"), capabilites);
		driver.manage().timeouts().implicitlyWait(10L, TimeUnit.SECONDS);
		System.out.println("Hello Thermos.");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		// driver.closeApp();
	}

	@Before
	public void setUp() throws Exception {

	}

	@After
	public void tearDown() throws Exception {

	}

	@Test
	public void homescreen() throws InterruptedException {
		assertTrue(isElementPresent(By.id("startButton")));
		assertTrue(isElementClickable(By.id("startButton")));		
		WebElement starbutton = driver.findElement(By.id("startButton"));
		starbutton.click();
		assertTrue(isElementPresent(By.id("cancel")));
		assertTrue(isElementClickable(By.id("cancel")));
		WebElement cancelbutton = driver.findElement(By.id("cancel"));
		cancelbutton.click();
		System.out.println("Run " + number + " times");
		while (true) {
			assertTrue(isElementPresent(By.id("startButton")));
			assertTrue(isElementClickable(By.id("startButton")));		
			starbutton.click();
			assertTrue(isElementPresent(By.id("cancel")));
			assertTrue(isElementClickable(By.id("cancel")));
			cancelbutton.click();
			number++;
			System.out.println("Run " + number + " times");
			if (number == 100) {
				break;
			}
		}
	}

	public boolean isElementPresent(By by) {
		try {
			driver.findElement(by);
			System.out.println("button is display.");
			return true;
		} catch (NoSuchElementException e) {
			System.out.println(e.getMessage());
			return false;
		}
	}

	public boolean isElementClickable(By by) {
		try {
			driver.findElement(by).isEnabled();
			System.out.println("button is clickable.");
			return true;
		} catch (NoSuchElementException e) {
			System.out.println(e.getMessage());
			return false;
		}
	}

}
